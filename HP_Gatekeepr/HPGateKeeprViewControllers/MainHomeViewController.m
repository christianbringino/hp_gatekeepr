//
//  MainHomeViewController.m
//  HP_Gatekeepr
//
//  Created by Christian Bringino on 6/4/21.
//  Copyright © 2021 Kristine Andrada. All rights reserved.
//

#import "MainHomeViewController.h"
#import "UserDefaultsHelper.h"
@import FirebaseRemoteConfig;

@interface MainHomeViewController ()
@property (weak, nonatomic) IBOutlet UIView *vwLine;
@property (weak, nonatomic) IBOutlet UIButton *btnVisitor;
@property (weak, nonatomic) IBOutlet UIButton *btnEmployee;
@property (weak, nonatomic) IBOutlet UIButton *btnExit;

@end

@implementation MainHomeViewController {
    FIRRemoteConfig *remoteConfig;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initViews];
}

- (void)initViews {
    self.btnVisitor.layer.borderWidth = 1;
    [self addRadiusWithView:self.btnExit radius:self.btnExit.bounds.size.width / 2 borderWidth:0 borderColor:UIColor.clearColor];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self applyColorsFromRemoteConfig];
}

- (void)applyColorsFromRemoteConfig {
    remoteConfig = [FIRRemoteConfig remoteConfig];
    NSString *strMainColor = [[remoteConfig configValueForKey:@"main_color"] stringValue];
    UIColor *mainColor = [self colorFromHexString:strMainColor];
    
    self.vwLine.backgroundColor = mainColor;
    self.btnEmployee.backgroundColor = mainColor;
    self.btnVisitor.layer.borderColor = mainColor.CGColor;
    [self.btnVisitor setTitleColor:mainColor forState:UIControlStateNormal];
    self.btnExit.backgroundColor = mainColor;
}

- (IBAction)didSegueToCompanyCode:(id)sender {
    [self createLogoutAlert];
}

-(void)createLogoutAlert{
    UIAlertController * logoutAlert =   [UIAlertController
                                         alertControllerWithTitle:@"Company Code"
                                         message:@"Are you sure you want to exit this page and proceed to company code?"
                                         preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* logoutAction = [UIAlertAction
                             actionWithTitle:@"YES"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [self logoutUser];
                             }];
    [logoutAlert addAction:logoutAction]; // add action to uialertcontroller
    UIAlertAction*cancelAction = [UIAlertAction
                            actionWithTitle:@"NO"
                            style:UIAlertActionStyleCancel
                            handler:^(UIAlertAction * action)
                            {
                            }];
    [logoutAlert addAction:cancelAction]; // add action to uialertcontroller
    [self presentViewController:logoutAlert animated:YES completion:nil];
}
-(void)logoutUser{
    [UserDefaultsHelper saveCodeAcceptedState:false];
    [UserDefaultsHelper saveCompanyCode:nil];
    [UserDefaultsHelper saveCompanyLoginType:nil];
    
    [self.view.window.rootViewController dismissViewControllerAnimated:true completion:nil];
}

@end
