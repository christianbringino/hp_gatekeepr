//
//  TemperatureViewController.m
//  HP_Gatekeepr
//
//  Created by Christian Bringino on 6/7/21.
//  Copyright © 2021 Kristine Andrada. All rights reserved.
//

#import "TemperatureViewController.h"
#import "Visitor.h"
#import "WebServicesManager.h"
#import "UserDefaultsHelper.h"
#import "AFNetworking.h"
@import FirebaseRemoteConfig;

@interface TemperatureViewController ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lcVwMainWidth;
@property (weak, nonatomic) IBOutlet UIView *vwStatusBar;
@property (weak, nonatomic) IBOutlet UIView *vwHeader;
@property (weak, nonatomic) IBOutlet UIView *vwContentHeader;
@property (weak, nonatomic) IBOutlet UITextField *tfTemperature;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;

@end

@implementation TemperatureViewController {
    FIRRemoteConfig *remoteConfig;
    BOOL isTemperatureValidated;
    CGFloat iPadHealthStatusViewWidth;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    iPadHealthStatusViewWidth = 440;
    
    [self initViews];
}

-(void) initViews {
    CGFloat screenWidth = UIScreen.mainScreen.bounds.size.width;
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) {
        self.lcVwMainWidth.constant = iPadHealthStatusViewWidth;
    } else {
        self.lcVwMainWidth.constant = screenWidth;
    }
    
    [self addRadiusWithView:self.vwContentHeader radius:20 borderWidth:0 borderColor:UIColor.clearColor];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.tfTemperature becomeFirstResponder];
    [self applyColorsFromRemoteConfig];
}

- (void)applyColorsFromRemoteConfig {
    remoteConfig = [FIRRemoteConfig remoteConfig];
    NSString *strMainColor = [[remoteConfig configValueForKey:@"main_color"] stringValue];
    UIColor *mainColor = [self colorFromHexString:strMainColor];
    
    [self addRadiusWithView:self.btnBack radius:10 borderWidth:0 borderColor:mainColor];
    [self addRadiusWithView:self.btnSubmit radius:10 borderWidth:0 borderColor:mainColor];
    
    self.vwStatusBar.backgroundColor = mainColor;
    self.vwHeader.backgroundColor = mainColor;
    self.btnSubmit.backgroundColor = mainColor;
    self.btnBack.backgroundColor = mainColor;
}

- (IBAction)didSubmit:(id)sender {
    [self validateTemperature];
    
    if (isTemperatureValidated) {
        [self showConsentAlert];
    }
}

- (void)validateTemperature {
    NSString *temperature = self.tfTemperature.text;
    
    isTemperatureValidated = false;
    
    if ([temperature length] == 0) {
        [self showAlertWithTitle:@"Temperature" message:@"Fields must not be empty"];
        return;
    }
    
    self.visitor.temperature = temperature;
    isTemperatureValidated = true;
}

- (void)showConsentAlert {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Consent" message:@"I hereby authorize Pointwest to collect and process the data indicated herein for the purpose of effecting control of the COVID-19 infection and contact tracing. I understand that my personal information is protected by RA 10173 Data Privacy Act of 2012, and that I am required by RA 11469, Bayanihan to Heal as One Act, to provide truthful information. I understand that this form will be destroyed after 30 days from the date of accomplishment, following the National Archives of the Philippines Protocol." preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *acceptAction = [UIAlertAction actionWithTitle:@"Accept" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self submitVisitorData];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:acceptAction];
    [alert addAction:cancelAction];
    [self presentViewController:alert animated:true completion:nil];
}

- (void)submitVisitorData {
    [self loadSpinner];
    NSString *url = @"https://docs.google.com/forms/d/e/1FAIpQLSfoFgMrshPyfqLxWpGAuPFPOxgIiCSwS1FAZwBdXkn_4CL9Eg/formResponse";
    
    NSDictionary *params = @{
                             V_FULL_NAME : self.visitor.fullName
                             ,V_ADDRESS: self.visitor.completeCurrentAddress
                             ,V_GENDER : self.visitor.sex
                             ,V_NUMBER : self.visitor.contactNumber
                             ,V_EMAIL : self.visitor.email
                             ,V_VISIT : self.visitor.natureOfVisit
                             ,V_COMPANY : self.visitor.company
                             ,V_COMPANY_ADDRESS : self.visitor.companyAddress
                             ,V_Q1 : self.visitor.questionOne
                             ,V_Q2 : self.visitor.questionTwo
                             ,V_Q3 : self.visitor.questionThree
                             ,V_Q4 : self.visitor.questionFour
                             ,V_Q5 : self.visitor.questionFive
                             ,V_Q5_LOC : self.visitor.questionFiveLoc
                             ,V_TEMPERATURE : self.visitor.temperature
                             };
    
    [[WebServicesManager sharedManager] postDataToSpreadsheet:url withDataParams:params withCallback:^(NSError *error) {
        [self stopSpinner];
        if (!error) {
            NSLog(@"%@", @"Success!");
            
            [self checkIfShowThankyouOrWarningMessageAlert];
        }else{
            if (![Reachability isInternetAvailable]) {
//                [self showErrorMessage: @"The internet connection appears to be offline." andTitle:@"Error"];
                [self showAlertWithTitle:@"Error" message:@"The internet connection appears to be offline."];
            }else{
//                 [self showErrorMessage: @"Registration Failed. Please try again." andTitle:@"Error"];
                [self showAlertWithTitle:@"Error" message:@"Registration Failed. Please try again."];
            }
        }
    }];
}

- (void)submitVisitorData2 {
    NSString *strUrl = @"https://docs.google.com/forms/d/e/1FAIpQLSfoFgMrshPyfqLxWpGAuPFPOxgIiCSwS1FAZwBdXkn_4CL9Eg/formResponse";
    NSURL *url = [NSURL URLWithString:strUrl];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    [urlRequest setHTTPMethod:@"POST"];
    
    NSString *fullName = [NSString stringWithFormat:@"entry.2132405323=%@&", self.visitor.fullName];
    NSString *address = [NSString stringWithFormat:@"entry.2143435968=%@&", self.visitor.completeCurrentAddress];
    NSString *gender = [NSString stringWithFormat:@"entry.248511995=%@&", self.visitor.sex];
    NSString *contactNumber = [NSString stringWithFormat:@"entry.1727020308=%@&", self.visitor.contactNumber];
    NSString *email = [NSString stringWithFormat:@"entry.1603298688=%@&", self.visitor.email];
    NSString *natureOfVisit = [NSString stringWithFormat:@"entry.565093738=%@&", self.visitor.natureOfVisit];
    NSString *company = [NSString stringWithFormat:@"entry.2010924722=%@&", self.visitor.company];
    NSString *companyAddress = [NSString stringWithFormat:@"entry.1248999853=%@&", self.visitor.companyAddress];
    NSString *q1 = [NSString stringWithFormat:@"entry.914362569=%@&", self.visitor.questionOne];
    NSString *q2 = [NSString stringWithFormat:@"entry.438266373=%@&", self.visitor.questionTwo];
    NSString *q3 = [NSString stringWithFormat:@"entry.1550166833=%@&", self.visitor.questionThree];
    NSString *q4 = [NSString stringWithFormat:@"entry.1664712767=%@&", self.visitor.questionFour];
    NSString *q5 = [NSString stringWithFormat:@"entry.1888207560=%@&", self.visitor.questionFive];
    NSString *q5Loc = [NSString stringWithFormat:@"entry.447420022=%@&", self.visitor.questionFiveLoc];
    NSString *temperature = [NSString stringWithFormat:@"entry.705442581=%@", self.visitor.temperature];
    
//    [postParamString stringByAppendingString:fullName];
//    [postParamString stringByAppendingString:address];
//    [postParamString stringByAppendingString:gender];
//    [postParamString stringByAppendingString:contactNumber];
//    [postParamString stringByAppendingString:email];
//    [postParamString stringByAppendingString:natureOfVisit];
//    [postParamString stringByAppendingString:company];
//    [postParamString stringByAppendingString:companyAddress];
//    [postParamString stringByAppendingString:q1];
//    [postParamString stringByAppendingString:q2];
//    [postParamString stringByAppendingString:q3];
//    [postParamString stringByAppendingString:q4];
//    [postParamString stringByAppendingString:q5];
//    [postParamString stringByAppendingString:q5Loc];
//    [postParamString stringByAppendingString:temperature];
    
    NSString *postParamString = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@", fullName, address, gender, contactNumber, email, natureOfVisit, company, companyAddress, q1, q2, q3, q4, q5, q5Loc, temperature];;
    
    [urlRequest setHTTPBody:[postParamString dataUsingEncoding:NSUTF8StringEncoding]];
    
    AFHTTPRequestOperationManager *operationManager = [AFHTTPRequestOperationManager manager];
    operationManager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [operationManager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    
    NSError *error = nil;
    
    [self loadSpinner];
    [[AFHTTPRequestSerializer serializer] requestBySerializingRequest:urlRequest withParameters:nil error:&error];
    [self stopSpinner];
    
    if (error == nil) {
        [self checkIfShowThankyouOrWarningMessageAlert];
    }
}

- (void)checkIfShowThankyouOrWarningMessageAlert {
    if (![self.visitor.questionOne  isEqual: @"None of the above"] || self.visitor.temperature.intValue >= 38) {
        [self showWarningMessageAlert];
    } else {
        [self showThankYouMessageAlert];
    }
}

- (void)showThankYouMessageAlert {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"\nThank You!" message:@"You may now enter Pointwest's premises" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [self.view.window.rootViewController dismissViewControllerAnimated:true completion:nil];
    }];
    
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(120, 5, 32, 32)];
    
    [imgView setImage:[UIImage imageNamed: @"ic_checkmark_green"]];
    [alert.view addSubview:imgView];
    [alert addAction:action];
    [self presentViewController:alert animated:true completion:nil];
}

- (void)showWarningMessageAlert {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"\nWarning" message:@"You may not enter Pointwest's premises. Please contact the security guard." preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [self.view.window.rootViewController dismissViewControllerAnimated:true completion:nil];
    }];
    
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(120, 5, 32, 32)];
    
    [imgView setImage:[UIImage imageNamed: @"img_red_cross"]];
    [alert.view addSubview:imgView];
    [alert addAction:action];
    [self presentViewController:alert animated:true completion:nil];
}

- (IBAction)didDismiss:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

@end
