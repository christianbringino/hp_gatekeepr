//
//  VisitorViewController.m
//  HP_Gatekeepr
//
//  Created by Christian Bringino on 6/7/21.
//  Copyright © 2021 Kristine Andrada. All rights reserved.
//

#import "Visitor.h"
#import "VisitorViewController.h"
#import "HealthQuestionnaireViewController.h"
@import FirebaseRemoteConfig;

@interface VisitorViewController ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lcVwMainWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lcVwCompanyHeight;
@property (weak, nonatomic) IBOutlet UIView *vwStatusBar;
@property (weak, nonatomic) IBOutlet UIView *vwHeader;
@property (weak, nonatomic) IBOutlet UIView *vwContentHeader;
@property (weak, nonatomic) IBOutlet UIView *vwCompany;
@property (weak, nonatomic) IBOutlet UITextField *tfFullName;
@property (weak, nonatomic) IBOutlet UITextField *tfAddress;
@property (weak, nonatomic) IBOutlet UITextField *tfEmail;
@property (weak, nonatomic) IBOutlet UITextField *tfContactNumber;
@property (weak, nonatomic) IBOutlet UITextField *tfCompany;
@property (weak, nonatomic) IBOutlet UITextField *tfCompanyAddress;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UIButton *btnMale;
@property (weak, nonatomic) IBOutlet UIButton *btnFemale;
@property (weak, nonatomic) IBOutlet UIButton *btnOfficial;
@property (weak, nonatomic) IBOutlet UIButton *btnPersonal;

@end

@implementation VisitorViewController {
    FIRRemoteConfig *remoteConfig;
    Visitor *visitor;
    CGFloat iPadHealthStatusViewWidth;
    UIColor *mainColor2;
    NSString *selectedGender;
    NSString *selectedNatureOfVisit;
    BOOL isTFValidated;
    BOOL isCompanyOptional;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    iPadHealthStatusViewWidth = 440;
    selectedGender = @"";
    selectedNatureOfVisit = @"";
    isCompanyOptional = true;
    
    [self initViews];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self applyColorsFromRemoteConfig];
}

- (void)initViews {
    CGFloat screenWidth = UIScreen.mainScreen.bounds.size.width;
    [self.vwCompany setHidden:true];
    self.lcVwCompanyHeight.constant = 0;
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) {
        self.lcVwMainWidth.constant = iPadHealthStatusViewWidth;
    } else {
        self.lcVwMainWidth.constant = screenWidth;
    }
    
    [self.btnMale setImage:nil forState:UIControlStateNormal];
    [self.btnFemale setImage:nil forState:UIControlStateNormal];
    [self.btnOfficial setImage:nil forState:UIControlStateNormal];
    [self.btnPersonal setImage:nil forState:UIControlStateNormal];
    [self addRadiusWithView:self.vwContentHeader radius:20 borderWidth:0 borderColor:UIColor.clearColor];
    
    
    [self updateViewConstraints];
}

- (void)applyColorsFromRemoteConfig {
    remoteConfig = [FIRRemoteConfig remoteConfig];
    NSString *strMainColor = [[remoteConfig configValueForKey:@"main_color"] stringValue];
    UIColor *mainColor = [self colorFromHexString:strMainColor];
    
    [self addRadiusWithView:self.btnBack radius:10 borderWidth:0 borderColor:mainColor];
    [self addRadiusWithView:self.btnNext radius:10 borderWidth:0 borderColor:mainColor];
    
    mainColor2 = mainColor;
    
    self.vwStatusBar.backgroundColor = mainColor;
    self.vwHeader.backgroundColor = mainColor;
    self.btnNext.backgroundColor = mainColor;
    self.btnBack.backgroundColor = mainColor;
    
    [self addRadiusWithView:self.btnMale radius:2 borderWidth:2 borderColor:mainColor];
    [self addRadiusWithView:self.btnFemale radius:2 borderWidth:2 borderColor:mainColor];
    [self addRadiusWithView:self.btnOfficial radius:2 borderWidth:2 borderColor:mainColor];
    [self addRadiusWithView:self.btnPersonal radius:2 borderWidth:2 borderColor:mainColor];
}

- (IBAction)didSelectMale:(id)sender {
    UIImage *image = [[UIImage imageNamed:@"img_checked"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    
    selectedGender = @"Male";
    
    [self.btnMale setImage:image forState:UIControlStateNormal];
    [self.btnMale.imageView setTintColor:mainColor2];
    
    [self.btnFemale setImage:nil forState:UIControlStateNormal];
}

- (IBAction)didSelectFemale:(id)sender {
    UIImage *image = [[UIImage imageNamed:@"img_checked"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    
    selectedGender = @"Female";
    
    [self.btnFemale setImage:image forState:UIControlStateNormal];
    [self.btnFemale.imageView setTintColor:mainColor2];
    [self.btnMale setImage:nil forState:UIControlStateNormal];
}

- (IBAction)didSelectNVOfficial:(id)sender {
    UIImage *image = [[UIImage imageNamed:@"img_checked"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    
    selectedNatureOfVisit = @"Official";
    isCompanyOptional = false;
    
    [self.btnOfficial setImage:image forState:UIControlStateNormal];
    [self.btnOfficial.imageView setTintColor:mainColor2];
    [self.btnPersonal setImage:nil forState:UIControlStateNormal];
    
    [self showCompanyFields];
}

- (void)showCompanyFields {
    [UIView animateWithDuration:0.5 animations:^{
        self.lcVwCompanyHeight.constant = 160;
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self.vwCompany setHidden:false];
    }];
}

- (void)hideCompanyFields {
    [self.vwCompany setHidden:true];
    
    [UIView animateWithDuration:0.5 animations:^{
        self.lcVwCompanyHeight.constant = 0;
        [self.view layoutIfNeeded];
    }];
}

- (IBAction)didSelectNVPersonal:(id)sender {
    UIImage *image = [[UIImage imageNamed:@"img_checked"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    
    selectedNatureOfVisit = @"Personal";
    isCompanyOptional = true;
    
    [self.btnPersonal setImage:image forState:UIControlStateNormal];
    [self.btnPersonal.imageView setTintColor:mainColor2];
    [self.btnOfficial setImage:nil forState:UIControlStateNormal];
    
    self.tfCompany.text = @"";
    self.tfCompanyAddress.text = @"";
    [self hideCompanyFields];
}

- (IBAction)didNextPage:(id)sender {
    [self validateTextfields];
    
    if (isTFValidated) {
        [self createVisitorObjectThenSegue];
    }
}

- (void)validateTextfields {
    NSString *fName = self.tfFullName.text;
    NSString *address = self.tfAddress.text;
    NSString *gender = selectedGender;
    NSString *contactNumber = self.tfContactNumber.text;
    NSString *email = self.tfEmail.text;
    NSString *natureOfVisit = selectedNatureOfVisit;
    NSString *company = self.tfCompany.text;
    NSString *companyAddress = self.tfCompanyAddress.text;
    
    isTFValidated = false;
    
    if ([fName length] == 0) {
        [self showAlertWithTitle:@"Full Name" message:@"Fields must not be empty"];
        return;
    }
    
    if ([address length] == 0) {
        [self showAlertWithTitle:@"Complete current Address" message:@"Fields must not be empty"];
        return;
    }
    
    if ([gender length] == 0) {
        [self showAlertWithTitle:@"Gender" message:@"Please select your gender"];
        return;
    }
    
    if ([contactNumber length] == 0) {
        [self showAlertWithTitle:@"Contact Number" message:@"Fields must not be empty"];
        return;
    }
    
    if ([email length] == 0) {
        [self showAlertWithTitle:@"Email" message:@"Fields must not be empty"];
        return;
    }
    
    if ([natureOfVisit length] == 0) {
        [self showAlertWithTitle:@"Nature of Visit" message:@"Please select your nature of visit"];
        return;
    }
    
    if (!isCompanyOptional) {
        if ([company length] == 0) {
            [self showAlertWithTitle:@"Company" message:@"Fields must not be empty"];
            return;
        }
        
        if ([companyAddress length] == 0) {
            [self showAlertWithTitle:@"Company Address" message:@"Fields must not be empty"];
            return;
        }
    }
    
    isTFValidated = true;
}

- (void)createVisitorObjectThenSegue {
    NSString *fName = self.tfFullName.text;
    NSString *address = self.tfAddress.text;
    NSString *gender = selectedGender;
    NSString *contactNumber = self.tfContactNumber.text;
    NSString *email = self.tfEmail.text;
    NSString *natureOfVisit = selectedNatureOfVisit;
    NSString *company = self.tfCompany.text;
    NSString *companyAddress = self.tfCompanyAddress.text;
    
    visitor = [[Visitor alloc] init];
    visitor.fullName = fName;
    visitor.completeCurrentAddress = address;
    visitor.sex = gender;
    visitor.contactNumber = contactNumber;
    visitor.email = email;
    visitor.natureOfVisit = natureOfVisit;
    visitor.company = company;
    visitor.companyAddress = companyAddress;
    
    [self segueToHealthQuestionnaire:visitor];
}

- (void)segueToHealthQuestionnaire: (Visitor *) visitor {
    [self performSegueWithIdentifier:@"showHealthQuestionnaire" sender:visitor];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    HealthQuestionnaireViewController *hvc = segue.destinationViewController;
    
    if (hvc != nil) {
        hvc.visitor = sender;
    }
}

- (IBAction)didDismiss:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

@end
