//
//  HealthAndVaccinationLegendViewController.m
//  HP_Gatekeepr
//
//  Created by Christian Bringino on 6/4/21.
//  Copyright © 2021 Kristine Andrada. All rights reserved.
//

@import FirebaseRemoteConfig;
@import FirebaseFunctions;
#import "HealthAndVaccinationLegendViewController.h"
#import "UserDefaultsHelper.h"
#import "ColorLegend.h"
#import "LegendCollectionViewCell.h"
#import "QRCodeReaderViewController.h"
#import "ScannedUserDetailsViewController.h"

@interface HealthAndVaccinationLegendViewController ()<UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, QRCodeReaderDelegate>
@property (weak, nonatomic) IBOutlet UIView *vwCompleteDose;
@property (weak, nonatomic) IBOutlet UIView *vwFirstDose;
@property (weak, nonatomic) IBOutlet UIView *vwNotVaccinated;
@property (weak, nonatomic) IBOutlet UIView *vwVaccinationLegend;
@property (weak, nonatomic) IBOutlet UIView *vwHealthLegend;
@property (weak, nonatomic) IBOutlet UIButton *btnScan;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lcHealthLegendWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lcVaccinationLegendWidth;

@end

@implementation HealthAndVaccinationLegendViewController {
    FIRRemoteConfig* remoteConfig;
    FIRDatabaseReference* ref;
    FIRFunctions *functions;
    CGFloat iPadHealthStatusViewWidth;
    
    NSMutableArray *colorLegends;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    ref = [[FIRDatabase database] reference];
    functions = [FIRFunctions functions];
    colorLegends = [[NSMutableArray alloc] init];
    iPadHealthStatusViewWidth = 400;
    
    [self fetchColorLegends];
}

- (void)fetchColorLegends {
    NSString* companyCode = [UserDefaultsHelper getCompanyCode];
    
    [self loadSpinner];
    [[[[[ref child:@"companyCodes"] child:companyCode] child:@"projectSettings"] child:@"colorLegend"] observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        [self stopSpinner];
        
        [self showViews];
        [self convertToColorLegendObjThenAddToArray:snapshot.value];
        NSLog(@"%@", snapshot.value);
    } withCancelBlock:^(NSError * _Nonnull error) {
        [self stopSpinner];
        // error
        [self showAlertWithTitle:@"Error" message:error.localizedDescription];
        NSLog(@"%@", error.localizedDescription);
    }];
}

- (void)showViews {
    [self.vwVaccinationLegend setHidden:false];
    [self.vwHealthLegend setHidden:false];
    [self.btnScan setHidden:false];
    [self.btnBack setHidden:false];
}

- (void)convertToColorLegendObjThenAddToArray: (NSArray*) dicArray {
    [colorLegends removeAllObjects];
    
    for (int i = 0; i < dicArray.count; i++) {
        NSDictionary *dic = dicArray[i];
        ColorLegend *colorLegend = [[ColorLegend alloc] initWithDataDictionary:dic];
        
        [colorLegends addObject:colorLegend];
        
        NSLog(@"%@", colorLegend);
    }
    
    [self.collectionView reloadData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self initViews];
    [self applyColorsFromRemoteConfig];
}

- (void)initViews {
    CGFloat screenWidth = UIScreen.mainScreen.bounds.size.width;
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) {
        self.lcHealthLegendWidth.constant = iPadHealthStatusViewWidth;
        self.lcVaccinationLegendWidth.constant = iPadHealthStatusViewWidth;
    } else {
        self.lcHealthLegendWidth.constant = screenWidth;
        self.lcVaccinationLegendWidth.constant = screenWidth;
    }
}

- (void)applyColorsFromRemoteConfig {
    remoteConfig = [FIRRemoteConfig remoteConfig];
    NSString *strMainColor = [[remoteConfig configValueForKey:@"main_color"] stringValue];
    UIColor *mainColor = [self colorFromHexString:strMainColor];
    
    
    self.btnScan.backgroundColor = mainColor;
    [self addRadiusWithView:self.vwCompleteDose radius:0 borderWidth:1 borderColor:mainColor];
    [self addRadiusWithView:self.vwFirstDose radius:0 borderWidth:1 borderColor:mainColor];
    [self addRadiusWithView:self.vwNotVaccinated radius:0 borderWidth:1 borderColor:mainColor];
    [self addRadiusWithView:self.btnScan radius:(self.btnScan.bounds.size.height / 2) borderWidth:0 borderColor:nil];
    [self addRadiusWithView:self.btnBack radius:10 borderWidth:0 borderColor:mainColor];
    
    self.btnBack.backgroundColor = mainColor;
}

- (IBAction)didOpenScanner:(id)sender {
    [self scanQRCodeWithSession];
}

-(void)scanQRCodeWithSession {
    if ([QRCodeReader supportsMetadataObjectTypes:@[AVMetadataObjectTypeQRCode]]) {
        static QRCodeReaderViewController *vc = nil;
        static dispatch_once_t onceToken;
        
        dispatch_once(&onceToken, ^{
            QRCodeReader *reader = [QRCodeReader readerWithMetadataObjectTypes:@[AVMetadataObjectTypeQRCode]];
            vc = [QRCodeReaderViewController readerWithCancelButtonTitle:@"Cancel" codeReader:reader startScanningAtLoad:YES showSwitchCameraButton:NO showTorchButton:YES];
            vc.modalPresentationStyle = UIModalPresentationFormSheet;
        });
        vc.delegate = self;
        
        [vc setCompletionWithBlock:^(NSString *resultAsString) {
            // NSLog(@"Completion with result: %@", resultAsString);
        }];
        
        [self presentViewController:vc animated:YES completion:NULL];
    }
    else {
        [self showAlertWithTitle:@"Error" message:@"Reader not supported by the current device"];
    }
}

- (void)reader:(QRCodeReaderViewController *)reader didScanResult:(NSString *)result {
    [self dismissViewControllerAnimated:YES completion:^{
        if ([result length] != 0) {
            [self fetchAttendeeData:result];
        }
    }];
}

- (void)fetchAttendeeData:(NSString *) userId {
    NSDictionary *params = @{@"userId": userId};
    
    [self loadSpinner];
    [[functions HTTPSCallableWithName:@"fetchAttendeeData"] callWithObject:params completion:^(FIRHTTPSCallableResult * _Nullable result, NSError * _Nullable error) {
        [self stopSpinner];
        
        if (error != nil) {
            if (error.code == 13) {
                [self showAlertWithTitle:@"Error" message:@"Patient not found"];
            } else {
                [self showAlertWithTitle:@"Error" message:error.localizedDescription];
            }
            return;
        }
        
        NSDictionary *dic = result.data;
        NSDictionary *dicData = [dic objectForKey:@"data"];
        NSString *strError = [dic objectForKey:@"error"];
        
        if (strError != nil) {
            [self showAlertWithTitle:@"Error" message:strError];
            return;
        }
        
        [self showPatientDetails:dicData];
//        [self postQRCodeInfoToSpreadsheet:dicData];
    }];
}

-(void)showPatientDetails: (NSDictionary *) dicData {
    NSDictionary *healthCheckData = [dicData objectForKey:@"healthCheckData"];
    NSString *userId = [dicData objectForKey:@"userId"];
    NSString *name = [dicData objectForKey:@"name"];
    NSString *imageId = [dicData objectForKey:@"imageId"];
    NSNumber *healthStatus = (NSNumber *) [healthCheckData objectForKey:@"status"];
    NSNumber *healthStatusTimeStamp = (NSNumber *) [healthCheckData objectForKey:@"timestamp"];
    
    ScannedUserDetailsViewController *sulvc = [self.storyboard instantiateViewControllerWithIdentifier:@"ScannedUserDetailsViewController"];
    sulvc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    sulvc.userId = userId;
    sulvc.name = name;
    sulvc.imageUrl = imageId;
    sulvc.healthStatus = healthStatus;
    sulvc.timestamp = healthStatusTimeStamp;
    
    [self presentViewController:sulvc animated:true completion:nil];
}

- (void)readerDidCancel:(QRCodeReaderViewController *)reader {
    [reader dismissViewControllerAnimated:true completion:nil];
}

- (IBAction)didDismiss:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return colorLegends.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    LegendCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LegendCollectionViewCell" forIndexPath:indexPath];
    ColorLegend *colorLegend = colorLegends[[indexPath row]];
    
    cell.lblName.text = colorLegend.name;
    cell.vwBackground.backgroundColor = [self colorFromHexString:colorLegend.colorHex];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) {
        CGFloat screenWidth = iPadHealthStatusViewWidth;
        
        return CGSizeMake(screenWidth / 2, 45);
    }
    
    CGFloat screenWidth = UIScreen.mainScreen.bounds.size.width;
    
    return CGSizeMake(screenWidth / 2, 45);
}

@end
