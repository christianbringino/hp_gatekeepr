//
//  HealthQuestionnaireViewController.h
//  HP_Gatekeepr
//
//  Created by Christian Bringino on 6/7/21.
//  Copyright © 2021 Kristine Andrada. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PWBaseViewController.h"
#import "Visitor.h"

NS_ASSUME_NONNULL_BEGIN

@interface HealthQuestionnaireViewController : PWBaseViewController
@property (strong, nonatomic) Visitor *visitor;

@end

NS_ASSUME_NONNULL_END
