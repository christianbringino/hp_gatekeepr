//
//  HealthQuestionnaireViewController.m
//  HP_Gatekeepr
//
//  Created by Christian Bringino on 6/7/21.
//  Copyright © 2021 Kristine Andrada. All rights reserved.
//

#import "HealthQuestionnaireViewController.h"
#import "Visitor.h"
#import "TemperatureViewController.h"
@import FirebaseRemoteConfig;

@interface HealthQuestionnaireViewController ()

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lcVwMainWidth;
//@property (strong, nonatomic) Visitor *visitor;
@property (weak, nonatomic) IBOutlet UIView *vwStatusBar;
@property (weak, nonatomic) IBOutlet UIView *vwHeader;
@property (weak, nonatomic) IBOutlet UIView *vwContentHeader;
@property (weak, nonatomic) IBOutlet UIView *vwOne;
@property (weak, nonatomic) IBOutlet UIView *vwTwo;
@property (weak, nonatomic) IBOutlet UIView *vwThree;
@property (weak, nonatomic) IBOutlet UIView *vwFour;
@property (weak, nonatomic) IBOutlet UIView *vwFive;
@property (weak, nonatomic) IBOutlet UITextField *tfSpecifyTravelDestination;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UIButton *btnQ1FirstAnswer;
@property (weak, nonatomic) IBOutlet UIButton *btnQ1SecondAnswer;
@property (weak, nonatomic) IBOutlet UIButton *btnQ1ThirdAnswer;
@property (weak, nonatomic) IBOutlet UIButton *btnQ1FourthAnswer;
@property (weak, nonatomic) IBOutlet UIButton *btnQ1FifthAnswer;
@property (weak, nonatomic) IBOutlet UIButton *btnQ2Yes;
@property (weak, nonatomic) IBOutlet UIButton *btnQ2No;
@property (weak, nonatomic) IBOutlet UIButton *btnQ3Yes;
@property (weak, nonatomic) IBOutlet UIButton *btnQ3No;
@property (weak, nonatomic) IBOutlet UIButton *btnQ4Yes;
@property (weak, nonatomic) IBOutlet UIButton *btnQ4No;
@property (weak, nonatomic) IBOutlet UIButton *btnQ5Yes;
@property (weak, nonatomic) IBOutlet UIButton *btnQ5No;

@end

@implementation HealthQuestionnaireViewController {
    FIRRemoteConfig *remoteConfig;
    UIColor *mainColor2;
    UIImage *checkedImage;
    BOOL isAnswersValited;
    CGFloat iPadHealthStatusViewWidth;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    checkedImage = [[UIImage imageNamed:@"img_checked"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    
    iPadHealthStatusViewWidth = 440;
    
    [self initViews];
}

- (void)initViews {
    CGFloat screenWidth = UIScreen.mainScreen.bounds.size.width;
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) {
        self.lcVwMainWidth.constant = iPadHealthStatusViewWidth;
    } else {
        self.lcVwMainWidth.constant = screenWidth;
    }
    
    [self addRadiusWithView:self.vwContentHeader radius:20 borderWidth:0 borderColor:UIColor.clearColor];
    
    [self.btnQ1FirstAnswer setImage:nil forState:UIControlStateNormal];
    [self.btnQ1SecondAnswer setImage:nil forState:UIControlStateNormal];
    [self.btnQ1ThirdAnswer setImage:nil forState:UIControlStateNormal];
    [self.btnQ1FourthAnswer setImage:nil forState:UIControlStateNormal];
    [self.btnQ1FifthAnswer setImage:nil forState:UIControlStateNormal];
    [self.btnQ2Yes setImage:nil forState:UIControlStateNormal];
    [self.btnQ2No setImage:nil forState:UIControlStateNormal];
    [self.btnQ3Yes setImage:nil forState:UIControlStateNormal];
    [self.btnQ3No setImage:nil forState:UIControlStateNormal];
    [self.btnQ4Yes setImage:nil forState:UIControlStateNormal];
    [self.btnQ4No setImage:nil forState:UIControlStateNormal];
    [self.btnQ5Yes setImage:nil forState:UIControlStateNormal];
    [self.btnQ5No setImage:nil forState:UIControlStateNormal];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self applyColorsFromRemoteConfig];
}

- (void)applyColorsFromRemoteConfig {
    remoteConfig = [FIRRemoteConfig remoteConfig];
    NSString *strMainColor = [[remoteConfig configValueForKey:@"main_color"] stringValue];
    UIColor *mainColor = [self colorFromHexString:strMainColor];
    
    mainColor2 = mainColor;
    
    self.vwStatusBar.backgroundColor = mainColor;
    self.vwHeader.backgroundColor = mainColor;
    self.btnNext.backgroundColor = mainColor;
    self.btnBack.backgroundColor = mainColor;
    self.vwOne.backgroundColor = mainColor;
    self.vwTwo.backgroundColor = mainColor;
    self.vwThree.backgroundColor = mainColor;
    self.vwFour.backgroundColor = mainColor;
    self.vwFive.backgroundColor = mainColor;
    
    [self addRadiusWithView:self.btnBack radius:10 borderWidth:0 borderColor:mainColor];
    [self addRadiusWithView:self.btnNext radius:10 borderWidth:0 borderColor:mainColor];
    [self addRadiusWithView:self.btnQ1FirstAnswer radius:2 borderWidth:2 borderColor:mainColor];
    [self addRadiusWithView:self.btnQ1SecondAnswer radius:2 borderWidth:2 borderColor:mainColor];
    [self addRadiusWithView:self.btnQ1ThirdAnswer radius:2 borderWidth:2 borderColor:mainColor];
    [self addRadiusWithView:self.btnQ1FourthAnswer radius:2 borderWidth:2 borderColor:mainColor];
    [self addRadiusWithView:self.btnQ1FifthAnswer radius:2 borderWidth:2 borderColor:mainColor];
    [self addRadiusWithView:self.btnQ2Yes radius:2 borderWidth:2 borderColor:mainColor];
    [self addRadiusWithView:self.btnQ2No radius:2 borderWidth:2 borderColor:mainColor];
    [self addRadiusWithView:self.btnQ3Yes radius:2 borderWidth:2 borderColor:mainColor];
    [self addRadiusWithView:self.btnQ3No radius:2 borderWidth:2 borderColor:mainColor];
    [self addRadiusWithView:self.btnQ4Yes radius:2 borderWidth:2 borderColor:mainColor];
    [self addRadiusWithView:self.btnQ4No radius:2 borderWidth:2 borderColor:mainColor];
    [self addRadiusWithView:self.btnQ5Yes radius:2 borderWidth:2 borderColor:mainColor];
    [self addRadiusWithView:self.btnQ5No radius:2 borderWidth:2 borderColor:mainColor];
}

- (IBAction)didNext:(id)sender {
    self.visitor.questionFiveLoc = self.tfSpecifyTravelDestination.text;
    
    [self validateAnswers];
    
    if (isAnswersValited) {
        [self updateVisitorThenSegue];
    }
}

- (void)validateAnswers {
    isAnswersValited = false;
    
    if (self.visitor.questionOne == nil || [self.visitor.questionOne length] == 0) {
        [self showAlertWithTitle:@"Error" message:@"Please answer your health questionnaire"];
        return;
    }
    
    if (self.visitor.questionTwo == nil || [self.visitor.questionTwo length] == 0) {
        [self showAlertWithTitle:@"Error" message:@"Please answer your health questionnaire"];
        return;
    }
    
    if (self.visitor.questionThree == nil || [self.visitor.questionThree length] == 0) {
        [self showAlertWithTitle:@"Error" message:@"Please answer your health questionnaire"];
        return;
    }
    
    if (self.visitor.questionFour == nil || [self.visitor.questionFour length] == 0) {
        [self showAlertWithTitle:@"Error" message:@"Please answer your health questionnaire"];
        return;
    }
    
    if (self.visitor.questionFive == nil || [self.visitor.questionFive length] == 0) {
        [self showAlertWithTitle:@"Error" message:@"Please answer your health questionnaire"];
        return;
    }
    
    isAnswersValited = true;
}

- (void)updateVisitorThenSegue {
    
    [self segueToTemperature];
}

- (void)segueToTemperature {
    [self performSegueWithIdentifier:@"showTemperature" sender:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    TemperatureViewController *tvc = segue.destinationViewController;
    
    if (tvc != nil) {
        tvc.visitor = self.visitor;
    }
}

- (IBAction)didDismiss:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}


// TOGGLE BUTTONS --------------------------------------

// Q1

- (IBAction)didSelectSoreThroat:(id)sender {
    [self clearQ1CheckBoxes];
    [self.btnQ1FirstAnswer setImage:checkedImage forState:UIControlStateNormal];
    [self.btnQ1FirstAnswer.imageView setTintColor:mainColor2];
    
    self.visitor.questionOne = @"Sore Throat (Masakit na lalamunan)";
}

- (void)clearQ1CheckBoxes {
    [self.btnQ1FirstAnswer setImage:nil forState:UIControlStateNormal];
    [self.btnQ1SecondAnswer setImage:nil forState:UIControlStateNormal];
    [self.btnQ1ThirdAnswer setImage:nil forState:UIControlStateNormal];
    [self.btnQ1FourthAnswer setImage:nil forState:UIControlStateNormal];
    [self.btnQ1FifthAnswer setImage:nil forState:UIControlStateNormal];
}

- (IBAction)didSelectBodyPains:(id)sender {
    [self clearQ1CheckBoxes];
    [self.btnQ1SecondAnswer setImage:checkedImage forState:UIControlStateNormal];
    [self.btnQ1SecondAnswer.imageView setTintColor:mainColor2];
    
    self.visitor.questionOne = @"Body Pains (Pananakit ng katawan)";
}

- (IBAction)didSelectHeadAche:(id)sender {
    [self clearQ1CheckBoxes];
    [self.btnQ1ThirdAnswer setImage:checkedImage forState:UIControlStateNormal];
    [self.btnQ1ThirdAnswer.imageView setTintColor:mainColor2];
    
    self.visitor.questionOne = @"Head Ache (Sakit ng ulo)";
}

- (IBAction)didSelectFever:(id)sender {
    [self clearQ1CheckBoxes];
    [self.btnQ1FourthAnswer setImage:checkedImage forState:UIControlStateNormal];
    [self.btnQ1FourthAnswer.imageView setTintColor:mainColor2];
    
    self.visitor.questionOne = @"Fever (Lagnat)";
}

- (IBAction)didSelectNoneOfTheAbove:(id)sender {
    [self clearQ1CheckBoxes];
    [self.btnQ1FifthAnswer setImage:checkedImage forState:UIControlStateNormal];
    [self.btnQ1FifthAnswer.imageView setTintColor:mainColor2];
    
    self.visitor.questionOne = @"None of the above";
}

// Q2

- (IBAction)didSelectQ2Yes:(id)sender {
    [self.btnQ2Yes setImage:checkedImage forState:UIControlStateNormal];
    [self.btnQ2Yes.imageView setTintColor:mainColor2];
    [self.btnQ2No setImage:nil forState:UIControlStateNormal];
    
    self.visitor.questionTwo = @"Yes (Oo)";
}

- (IBAction)didSelectQ2No:(id)sender {
    [self.btnQ2No setImage:checkedImage forState:UIControlStateNormal];
    [self.btnQ2No.imageView setTintColor:mainColor2];
    [self.btnQ2Yes setImage:nil forState:UIControlStateNormal];
    
    self.visitor.questionTwo = @"No (Hindi)";
}

// Q3

- (IBAction)didSelectQ3Yes:(id)sender {
    [self.btnQ3Yes setImage:checkedImage forState:UIControlStateNormal];
    [self.btnQ3Yes.imageView setTintColor:mainColor2];
    [self.btnQ3No setImage:nil forState:UIControlStateNormal];
    
    self.visitor.questionThree = @"Yes (Oo)";
}

- (IBAction)didSelectQ3No:(id)sender {
    [self.btnQ3No setImage:checkedImage forState:UIControlStateNormal];
    [self.btnQ3No.imageView setTintColor:mainColor2];
    [self.btnQ3Yes setImage:nil forState:UIControlStateNormal];
    
    self.visitor.questionThree = @"No (Hindi)";
}

// Q4

- (IBAction)didSelectQ4Yes:(id)sender {
    [self.btnQ4Yes setImage:checkedImage forState:UIControlStateNormal];
    [self.btnQ4Yes.imageView setTintColor:mainColor2];
    [self.btnQ4No setImage:nil forState:UIControlStateNormal];
    
    self.visitor.questionFour = @"Yes (Oo)";
}

- (IBAction)didSelectQ4No:(id)sender {
    [self.btnQ4No setImage:checkedImage forState:UIControlStateNormal];
    [self.btnQ4No.imageView setTintColor:mainColor2];
    [self.btnQ4Yes setImage:nil forState:UIControlStateNormal];
    
    self.visitor.questionFour = @"No (Hindi)";
}

// Q5

- (IBAction)didSelectQ5Yes:(id)sender {
    [self.btnQ5Yes setImage:checkedImage forState:UIControlStateNormal];
    [self.btnQ5Yes.imageView setTintColor:mainColor2];
    [self.btnQ5No setImage:nil forState:UIControlStateNormal];
    
    self.visitor.questionFive = @"Yes (Oo)";
}

- (IBAction)didSelectQ5No:(id)sender {
    [self.btnQ5No setImage:checkedImage forState:UIControlStateNormal];
    [self.btnQ5No.imageView setTintColor:mainColor2];
    [self.btnQ5Yes setImage:nil forState:UIControlStateNormal];
    
    self.visitor.questionFive = @"No (Hindi)";
}

// TOGGLE BUTTONS --------------------------------------

@end
