//
//  MainHomeViewController.h
//  HP_Gatekeepr
//
//  Created by Christian Bringino on 6/4/21.
//  Copyright © 2021 Kristine Andrada. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MainHomeViewController : PWBaseViewController

@end

NS_ASSUME_NONNULL_END
