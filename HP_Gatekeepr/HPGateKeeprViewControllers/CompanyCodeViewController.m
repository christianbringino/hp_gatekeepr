//
//  CompanyCodeViewController.m
//  HP_Gatekeepr
//
//  Created by Christian Bringino on 5/27/21.
//  Copyright © 2021 Kristine Andrada. All rights reserved.
//

#import "CompanyCodeViewController.h"
#import "UserDefaultsHelper.h"
@import FirebaseDatabase;
@import FirebaseRemoteConfig;

@interface CompanyCodeViewController ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *tfCompanyCode1;
@property (weak, nonatomic) IBOutlet UITextField *tfCompanyCode2;
@property (weak, nonatomic) IBOutlet UITextField *tfCompanyCode3;
@property (weak, nonatomic) IBOutlet UITextField *tfCompanyCode4;
@property (weak, nonatomic) IBOutlet UITextField *tfCompanyCode5;
@property (weak, nonatomic) IBOutlet UITextField *tfCompanyCode6;
@property (weak, nonatomic) IBOutlet UIButton *btnVerify;
@property (strong, nonatomic) FIRDatabaseReference *ref;

@end

@implementation CompanyCodeViewController {
    FIRRemoteConfig *remoteConfig;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.ref = [[FIRDatabase database] reference];
    
    [self addTargets];
}

- (void)addTargets {
    [self.tfCompanyCode1 addTarget:self action:@selector(didTextFieldChanged:) forControlEvents:UIControlEventEditingChanged];
    [self.tfCompanyCode2 addTarget:self action:@selector(didTextFieldChanged:) forControlEvents:UIControlEventEditingChanged];
    [self.tfCompanyCode3 addTarget:self action:@selector(didTextFieldChanged:) forControlEvents:UIControlEventEditingChanged];
    [self.tfCompanyCode4 addTarget:self action:@selector(didTextFieldChanged:) forControlEvents:UIControlEventEditingChanged];
    [self.tfCompanyCode5 addTarget:self action:@selector(didTextFieldChanged:) forControlEvents:UIControlEventEditingChanged];
    [self.tfCompanyCode6 addTarget:self action:@selector(didTextFieldChanged:) forControlEvents:UIControlEventEditingChanged];
}

- (void)didTextFieldChanged: (UITextField *) textfield {
    switch (textfield.tag) {
        case 1:
            if (textfield.text.length >= 1) {
                [_tfCompanyCode2 becomeFirstResponder];
            }
            break;
        case 2:
            if (textfield.text.length >= 1) {
                [_tfCompanyCode3 becomeFirstResponder];
            } else if (textfield.text.length <= 0) {
                [_tfCompanyCode1 becomeFirstResponder];
            }
            break;
        case 3:
            if (textfield.text.length >= 1) {
                [_tfCompanyCode4 becomeFirstResponder];
            } else if (textfield.text.length <= 0) {
                [_tfCompanyCode2 becomeFirstResponder];
            }
            break;
        case 4:
            if (textfield.text.length >= 1) {
                [_tfCompanyCode5 becomeFirstResponder];
            } else if (textfield.text.length <= 0) {
                [_tfCompanyCode3 becomeFirstResponder];
            }
            break;
        case 5:
            if (textfield.text.length >= 1) {
                [_tfCompanyCode6 becomeFirstResponder];
            } else if (textfield.text.length <= 0) {
                [_tfCompanyCode4 becomeFirstResponder];
            }
            break;
        case 6:
            if (textfield.text.length >= 1) {
                [_tfCompanyCode6 resignFirstResponder];
            } else if (textfield.text.length <= 0) {
                [_tfCompanyCode5 becomeFirstResponder];
            }
            break;
        default:
            break;
    }
    
    [self checkIfTextFieldsAreEmpty];
}

- (void)checkIfTextFieldsAreEmpty {
    NSString* cc1 = _tfCompanyCode1.text;
    NSString* cc2 = _tfCompanyCode2.text;
    NSString* cc3 = _tfCompanyCode3.text;
    NSString* cc4 = _tfCompanyCode4.text;
    NSString* cc5 = _tfCompanyCode5.text;
    NSString* cc6 = _tfCompanyCode6.text;
    
    if ([cc1 length] != 0 && [cc2 length] != 0 && [cc3 length] != 0 && [cc4 length] != 0 && [cc5 length] != 0 && [cc6 length] != 0) {
        [self.btnVerify setEnabled:true];
        [self.btnVerify setAlpha:1.0];
    } else {
        [self.btnVerify setEnabled:false];
        [self.btnVerify setAlpha:0.54];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self applyColorsFromRemoteConfig];
    [self checkIfTextFieldsAreEmpty];
}

- (void)applyColorsFromRemoteConfig {
    remoteConfig = [FIRRemoteConfig remoteConfig];
    NSString *strMainColor = [[remoteConfig configValueForKey:@"main_color"] stringValue];
    UIColor *mainColor = [self colorFromHexString:strMainColor];
    
    self.btnVerify.backgroundColor = mainColor;
}

- (IBAction)didVerifyCompanyCode:(id)sender {
    NSString* cc1 = _tfCompanyCode1.text;
    NSString* cc2 = _tfCompanyCode2.text;
    NSString* cc3 = _tfCompanyCode3.text;
    NSString* cc4 = _tfCompanyCode4.text;
    NSString* cc5 = _tfCompanyCode5.text;
    NSString* cc6 = _tfCompanyCode6.text;
    
    NSString* companyCode = [NSString stringWithFormat:@"%@%@%@%@%@%@", cc1, cc2, cc3, cc4, cc5, cc6];
    
    [self verifyCompanyCode:companyCode];
}

- (void)verifyCompanyCode:(NSString *) companyCode {
    [self loadSpinner];
    [[[self.ref child:@"companyCodes"] child:companyCode] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        [self stopSpinner];
        
        if (snapshot.value == [NSNull null]) {
            [self showAlertWithTitle:@"Error" message:@"Company Code does not exist"];
            return;
        }
        
        NSDictionary* dic = snapshot.value;
        NSString* companyLoginType = dic[@"loginType"];
        NSString* companyCode = dic[@"companyCode"];
        
        [UserDefaultsHelper saveCompanyCode:companyCode];
        [UserDefaultsHelper saveCompanyLoginType: companyLoginType];
        [self clearTextfields];
        [self performSegueWithIdentifier:@"showEmployeeVisitor" sender:nil];
    } withCancelBlock:^(NSError * _Nonnull error) {
        [self stopSpinner];
        [self showAlertWithTitle:@"Error" message:error.localizedDescription];
    }];
}

- (void)clearTextfields {
    _tfCompanyCode1.text = @"";
    _tfCompanyCode2.text = @"";
    _tfCompanyCode3.text = @"";
    _tfCompanyCode4.text = @"";
    _tfCompanyCode5.text = @"";
    _tfCompanyCode6.text = @"";
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if ([textField.text length] == [string length]) {
        switch (textField.tag) {
            case 1:
                [_tfCompanyCode2 becomeFirstResponder];
                break;
            case 2:
                [_tfCompanyCode3 becomeFirstResponder];
                break;
            case 3:
                [_tfCompanyCode4 becomeFirstResponder];
                break;
            case 4:
                [_tfCompanyCode5 becomeFirstResponder];
                break;
            case 5:
                [_tfCompanyCode6 becomeFirstResponder];
                break;
            default:
                break;
        }
        
        return false;
    }
    
    return true;
}

@end
