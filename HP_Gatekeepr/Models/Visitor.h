//
//  Visitor.h
//  HP_Gatekeepr
//
//  Created by Christian Bringino on 6/7/21.
//  Copyright © 2021 Kristine Andrada. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Visitor : NSObject
@property (nonatomic, strong) NSString *fullName;
@property (nonatomic, strong) NSString *completeCurrentAddress;
@property (nonatomic, strong) NSString *sex;
@property (nonatomic, strong) NSString *contactNumber;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *natureOfVisit;
@property (nonatomic, strong) NSString *company;
@property (nonatomic, strong) NSString *companyAddress;
@property (nonatomic, strong) NSString *questionOne;
@property (nonatomic, strong) NSString *questionTwo;
@property (nonatomic, strong) NSString *questionThree;
@property (nonatomic, strong) NSString *questionFour;
@property (nonatomic, strong) NSString *questionFive;
@property (nonatomic, strong) NSString *questionFiveLoc;
@property (nonatomic, strong) NSString *temperature;

@end

NS_ASSUME_NONNULL_END
