//
//  LegendCollectionViewCell.h
//  HP_Gatekeepr
//
//  Created by Christian Bringino on 6/4/21.
//  Copyright © 2021 Kristine Andrada. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LegendCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIView *vwBackground;
@property (weak, nonatomic) IBOutlet UILabel *lblName;

@end

NS_ASSUME_NONNULL_END
