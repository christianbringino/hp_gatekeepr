//
//  Notifications.m
//  HP Gatekeepr
//
//  Created by Christian Bringino on 8/25/20.
//  Copyright © 2020 Kristine Andrada. All rights reserved.
//

#import "Notifications.h"

@implementation Notifications
- (instancetype)initWithDataDictionary:(NSDictionary *)dataDict {
    self.notifTitle = [dataDict objectForKey:@"notifTitle"];
    self.notifMessage = [dataDict objectForKey:@"notifMessage"];
    
//    self.notifTimestamp = [dataDict objectForKey:@"notifTimestamp"];
    NSNumber *timestamp = [dataDict objectForKey:@"notifTimestamp"];
    NSDate *date = [[NSDate alloc] initWithTimeIntervalSince1970:timestamp.doubleValue];
    
    self.notifTimestamp = [self fromDateToString:date format:@"MM/dd/yyyy \n hh:mm a"];
    
    return self;
}

- (NSString *)fromDateToString:(NSDate *) date format :(NSString *) format {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = format;
    return [dateFormatter stringFromDate:date];
}

@end
