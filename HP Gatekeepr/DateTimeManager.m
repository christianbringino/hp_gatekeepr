//
//  DateTimeManager.m
//  SpeakerQuickView
//
//  Created by Aljon Antiola on 6/19/15.
//  Copyright (c) 2015 Training 1. All rights reserved.
//
#import "AFNetworking.h"
#import "DateTimeManager.h"
#import "PWGoogleSpreadsheetParser.h"
#import "LocalDataManager.h"



@implementation DateTimeManager

+(BOOL)isNewDate:(NSDate *)newDate newerThanOldDate:(NSDate *)oldDate{
    if ([newDate compare:oldDate] == NSOrderedSame) {
        return  NO;
    }else if([newDate compare:oldDate] == NSOrderedAscending){
        return NO;
    }else{
        return YES;
    }
}
+(NSDate *)getDateFromStr:(NSString *)dateStr{
    NSDate *date;
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc]init];
    dateFormatter.timeZone = [NSTimeZone localTimeZone];
    dateFormatter.dateFormat = @"MM/dd/yyyy";
    date = [dateFormatter dateFromString:dateStr];
    if (date) {
        return date;
    }
    dateFormatter.dateFormat = @"MM/dd/yyyy HH:mm";
    date = [dateFormatter dateFromString:dateStr];
    if (date) {
        return date;
    }
    
    dateFormatter.dateFormat = @"MM/dd/yyyy HH:mm:ss";
    date = [dateFormatter dateFromString:dateStr];
    if (date) {
        return date;
    }
    dateFormatter.dateFormat = @"MM/dd/yyyy hh:mm a";
    date = [dateFormatter dateFromString:dateStr];
    if (date) {
        return date;
    }
    dateFormatter.dateFormat = @"MM/dd/yyyy hh:mm:ss a";
    date = [dateFormatter dateFromString:dateStr];
    if (date) {
        return date;
    }
    dateFormatter.dateFormat = @"MM/dd/yyyy hh:mm:ss a";
    date = [dateFormatter dateFromString:dateStr];
    if (date) {
        return date;
    }
    
    dateFormatter.dateFormat = @"MM-dd-yyyy HH:mm";
    date = [dateFormatter dateFromString:dateStr];
    if (date) {
        return date;
    }
    
    dateFormatter.dateFormat = @"MM-dd-yyyy HH:mm:ss";
    date = [dateFormatter dateFromString:dateStr];
    if (date) {
        return date;
    }
    dateFormatter.dateFormat = @"MM-dd-yyyy hh:mm a";
    date = [dateFormatter dateFromString:dateStr];
    if (date) {
        return date;
    }
    dateFormatter.dateFormat = @"MM-dd-yyyy hh:mm:ss a";
    date = [dateFormatter dateFromString:dateStr];
    if (date) {
        return date;
    }
    dateFormatter.dateFormat = @"MM-dd-yyyy hh:mm:ss a";
    date = [dateFormatter dateFromString:dateStr];
    if (date) {
        return date;
    }
    dateFormatter.dateFormat = @"EEE, dd MMM yyyy HH:mm:ss Z";
    date = [dateFormatter dateFromString:dateStr];
    if (date) {
        return date;
    }
    dateFormatter.dateFormat = @"EEE MMM dd HH:mm:ss yyyy";
    date = [dateFormatter dateFromString:dateStr];
    if (date) {
        return date;
    }
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    date = [dateFormatter dateFromString:dateStr];
    if (date) {
        return date;
    }
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss Z";
    date = [dateFormatter dateFromString:dateStr];
    if (date) {
        return date;
    }
    dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ssZ";
    date = [dateFormatter dateFromString:dateStr];
    if (date) {
        return date;
    }
    dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    date = [dateFormatter dateFromString:dateStr];
    if (date) {
        return date;
    }
    dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss Z";
    date = [dateFormatter dateFromString:dateStr];
    if (date) {
        return date;
    }
    dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ssZZZZZ";
    date = [dateFormatter dateFromString:dateStr];
    if (date) {
        return date;
    }
    
    return date;
    
}
+(NSString *)getStringFromDate:(NSDate *)date{
    NSString *dateStr = @"";
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc]init];
    dateFormatter.timeZone = [NSTimeZone localTimeZone];
    dateFormatter.dateFormat = @"MM/dd/yyyy hh:mm a";
    dateStr =  [dateFormatter stringFromDate:date];
    return dateStr;
}
+(NSString *)getDateOnlyFromDate:(NSDate *)date{
    NSString *dateStr = @"";
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc]init];
    dateFormatter.timeZone = [NSTimeZone localTimeZone];
    dateFormatter.dateFormat = @"MM/dd/yyyy";
    dateStr =  [dateFormatter stringFromDate:date];
    return dateStr;
}
+(NSString *)getTimeOnlyFromDate:(NSDate *)date{
    NSString *dateStr = @"";
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc]init];
    dateFormatter.timeZone = [NSTimeZone localTimeZone];
    dateFormatter.dateFormat = @"h:mm a";
    dateStr =  [dateFormatter stringFromDate:date];
    return dateStr;
}
+(NSArray *)sortDateFromMostRecent:(NSArray *)dateArray{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
    
    NSMutableArray *dates = [NSMutableArray arrayWithCapacity:dateArray.count];
    for (NSString *dateString in dateArray)
    {
        NSDate *date = [dateFormatter dateFromString:dateString];
        [dates addObject:date];
    }
    
    [dates sortUsingSelector:@selector(compare:)];
    
    NSMutableArray *sortedDate = [NSMutableArray arrayWithCapacity:dates.count];
    for (NSDate *date in dates)
    {
        NSString *timeString = [dateFormatter stringFromDate:date];
        [sortedDate addObject:timeString];
    }
    
    return sortedDate;
}


@end
