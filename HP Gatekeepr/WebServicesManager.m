//
//  WebServicesManager.m
//  Pocket Rocket
//
//  Created by ios-training on 6/22/15.
//  Copyright (c) 2015 ios-training. All rights reserved.
//

#import "WebServicesManager.h"

@implementation WebServicesManager

+ (id)sharedManager {
    static WebServicesManager *sharedMyManager = nil;
    @synchronized(self) {
        if (sharedMyManager == nil)
            sharedMyManager = [[self alloc] init];
    }
    return sharedMyManager;
}

- (NSOperationQueue *) postDataToSpreadsheet:(NSString *)url
                              withDataParams:(NSDictionary *)params
                                withCallback:(void(^)(NSError *error))callBack {
    
    AFHTTPRequestOperationManager *operationManager = [AFHTTPRequestOperationManager manager];
    operationManager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [operationManager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    
    [operationManager POST:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        callBack(nil);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callBack(error);
    }];
    
    return operationManager.operationQueue;
}


- (NSOperationQueue *) getDataFromSpreadsheet:(NSString *)url
                                withCallback:(void(^)(NSDictionary *data, NSError *error))callBack {
    
    AFHTTPRequestOperationManager *operationManager = [AFHTTPRequestOperationManager manager];
    operationManager.requestSerializer = [AFHTTPRequestSerializer serializer];
    
    operationManager.responseSerializer.acceptableContentTypes = [operationManager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    
    [operationManager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        callBack(responseObject, nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callBack(nil,error);
    }];
    
    
    return operationManager.operationQueue;
}


@end
