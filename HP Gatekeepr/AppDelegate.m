//
//  AppDelegate.m
//  EESY Breakout
//
//  Created by Kristine Andrada on 10/13/16.
//  Copyright © 2016 Kristine Andrada. All rights reserved.
//

#import "AppDelegate.h"
#import "Constants.h"
#import "WebServicesManager.h"
#import "LocalDataManager.h"
#import "DateTimeManager.h"
#import "PWGoogleSpreadsheetParser.h"
#import "SessionListViewController.h"
#import "HomeViewController.h"
#import "UserDefaultsHelper.h"
@import Firebase;
@import FirebaseMessaging;

@interface AppDelegate ()<UNUserNotificationCenterDelegate, FIRMessagingDelegate>

@end

@implementation AppDelegate {
    NSString *kGCMMessageIDKey;
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
//    BOOL hasCodeBeenAccepted = [UserDefaultsHelper getCodeAcceptedState];
//    if (hasCodeBeenAccepted) {
//        [self changeRootViewToSessionList];
//    }else{
//         [self changeRootViewToHome];
//    }
    
    kGCMMessageIDKey = @"gcm.message_id";
    
    [FIRApp configure];
    [FIRMessaging messaging].delegate = self;
    [[FIRMessaging messaging] subscribeToTopic:@"pointwest_hp_notif"];
    
    if ([UNUserNotificationCenter class] != nil) {
      // iOS 10 or later
      // For iOS 10 display notification (sent via APNS)
      [UNUserNotificationCenter currentNotificationCenter].delegate = self;
      UNAuthorizationOptions authOptions = UNAuthorizationOptionAlert |
          UNAuthorizationOptionSound | UNAuthorizationOptionBadge;
      [[UNUserNotificationCenter currentNotificationCenter]
          requestAuthorizationWithOptions:authOptions
          completionHandler:^(BOOL granted, NSError * _Nullable error) {
            // ...
          }];
    } else {
      // iOS 10 notifications aren't available; fall back to iOS 8-9 notifications.
      UIUserNotificationType allNotificationTypes =
      (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
      UIUserNotificationSettings *settings =
      [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
      [application registerUserNotificationSettings:settings];
    }

    [application registerForRemoteNotifications];
    
//    static NSString* const hasRunAppOnceKey = @"hasRunAppOnceKey";
//    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
//    if ([defaults boolForKey:hasRunAppOnceKey] == NO)
//    {
//        // Some code you want to run on first use...
//        [defaults setBool:YES forKey:hasRunAppOnceKey];
//    }
    // Override point for customization after application launch.
    return YES;
}
- (UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
    return UIInterfaceOrientationMaskPortrait;
}

-(void)changeRootViewToSessionList{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SessionListViewController *slvc = [storyboard instantiateViewControllerWithIdentifier:@"SessionListViewController"];
    UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:slvc];
    self.window.rootViewController = navController;
   // [[UIApplication sharedApplication].keyWindow setRootViewController:slvc];

}
-(void)changeRootViewToHome{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    HomeViewController *hvc = [storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
    UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:hvc];
    self.window.rootViewController = navController;
   // [[UIApplication sharedApplication].keyWindow setRootViewController:hvc];
    
}

- (void)messaging:(FIRMessaging *)messaging didReceiveRegistrationToken:(NSString *)fcmToken {
    NSLog(@"FCM registration token: %@", fcmToken);
    // Notify about received token.
    NSDictionary *dataDict = [NSDictionary dictionaryWithObject:fcmToken forKey:@"token"];
    [[NSNotificationCenter defaultCenter] postNotificationName:
     @"FCMToken" object:nil userInfo:dataDict];
    // TODO: If necessary send token to application server.
    // Note: This callback is fired at each app startup and whenever a new token is generated.
}

// With "FirebaseAppDelegateProxyEnabled": NO
- (void)application:(UIApplication *)application
    didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    [FIRMessaging messaging].APNSToken = deviceToken;
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
  // If you are receiving a notification message while your app is in the background,
  // this callback will not be fired till the user taps on the notification launching the application.
  // TODO: Handle data of notification

  // With swizzling disabled you must let Messaging know about the message, for Analytics
  // [[FIRMessaging messaging] appDidReceiveMessage:userInfo];

  // Print message ID.
  if (userInfo[kGCMMessageIDKey]) {
    NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
  }

  // Print full message.
  NSLog(@"%@", userInfo);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
    fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
  // If you are receiving a notification message while your app is in the background,
  // this callback will not be fired till the user taps on the notification launching the application.
  // TODO: Handle data of notification

  // With swizzling disabled you must let Messaging know about the message, for Analytics
  // [[FIRMessaging messaging] appDidReceiveMessage:userInfo];

  // Print message ID.
  if (userInfo[kGCMMessageIDKey]) {
    NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
  }

  // Print full message.
  NSLog(@"%@", userInfo);

  completionHandler(UIBackgroundFetchResultNewData);
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
