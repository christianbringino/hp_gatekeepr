//
//  RoomsViewController.m
//  HP Gatekeepr
//
//  Created by Christian Bringino on 8/25/20.
//  Copyright © 2020 Kristine Andrada. All rights reserved.
//

#import "Room.h"
#import "RoomTableViewCell.h"
#import "RoomsViewController.h"
#import "WebServicesManager.h"
#import "LocalDataManager.h"
#import "AppDelegate.h"
#import "LegendViewController.h"
#import "UserDefaultsHelper.h"

@interface RoomsViewController ()<UITableViewDelegate, UITableViewDataSource>

@end

@implementation RoomsViewController {
    NSMutableArray *arrRooms;
    NSString *selectedRoom;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    arrRooms = [[NSMutableArray alloc] init];
    [self createLoadingScreen];
    [self fetchRooms];
}

- (void)fetchRooms {
    NSString *url = @"https://spreadsheets.google.com/feeds/list/1ZGcXei_aeFc2RxIO7W25CawIJDE82azuLhVBnLaaNhg/1/public/values?alt=json";
        
    [self loadSpinner];
    [[WebServicesManager sharedManager] getDataFromSpreadsheet:url withCallback:^(NSDictionary *data, NSError *error) {
        [self stopSpinner];
        
        if ((error == nil) && (data != nil)) {
            NSDictionary *feed = [data objectForKey:@"feed"];
            NSArray<NSDictionary *> *arrDic = [feed objectForKey:@"entry"];
            
            for (NSDictionary *dic in arrDic) {
                Room *room = [[Room alloc] initWithDataDictionary:dic];
                
                [arrRooms addObject:room];
            }
            
            [self.tableView reloadData];
        } else {
            [self showAlertWithTitle:@"Error" message:error.localizedDescription];
        }
    }];
}

- (IBAction)didLogout:(id)sender {
    [self createLogoutAlert];
}

-(void)createLogoutAlert{
    UIAlertController * logoutAlert =   [UIAlertController
                                         alertControllerWithTitle:@"Log Out"
                                         message:@"This will delete all the data stored in the app. Are you sure you want to log out?"
                                         preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* logoutAction = [UIAlertAction
                             actionWithTitle:@"LOGOUT"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [self logoutUser];
                             }];
    [logoutAlert addAction:logoutAction]; // add action to uialertcontroller
    UIAlertAction*cancelAction = [UIAlertAction
                            actionWithTitle:@"CANCEL"
                            style:UIAlertActionStyleCancel
                            handler:^(UIAlertAction * action)
                            {
                            }];
    [logoutAlert addAction:cancelAction]; // add action to uialertcontroller
    [self presentViewController:logoutAlert animated:YES completion:nil];
}
-(void)logoutUser{
    
//    [LocalDataManager removeFilesInDirectory];
//    NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
//    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
    
    [UserDefaultsHelper saveCodeAcceptedState:false];
    [self.view.window.rootViewController dismissViewControllerAnimated:true completion:nil];
    
//    self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
//    [(AppDelegate *)[[UIApplication sharedApplication] delegate] changeRootViewToHome];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.destinationViewController isKindOfClass:[LegendViewController class]]) {
        LegendViewController *vc = segue.destinationViewController;
        vc.selectedRoom = selectedRoom;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrRooms.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    RoomTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RoomTableViewCell" forIndexPath:indexPath];
    Room *room = arrRooms[indexPath.row];
    
    cell.lblRoom.text = room.room;
    cell.lblDescription.text = room.descriptions;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Room *room = arrRooms[indexPath.row];
    selectedRoom = room.room;
    
    [self performSegueWithIdentifier:@"showLegend" sender:nil];
}

@end
