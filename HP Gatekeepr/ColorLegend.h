//
//  ProjectSettings.h
//  HP Scan
//
//  Created by Christian Bringino on 6/26/20.
//  Copyright © 2020 Kristine Andrada. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ColorLegend : NSObject
@property (nonatomic, assign) NSInteger *identifier;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *colorHex;

-(instancetype)initWithDataDictionary:(NSDictionary *)dataDict;
@end

NS_ASSUME_NONNULL_END
