//
//  WebServicesManager.h
//  Pocket Rocket
//
//  Created by ios-training on 6/22/15.
//  Copyright (c) 2015 ios-training. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

@interface WebServicesManager : NSObject

/// Singleton for web requests
+ (id)sharedManager;


/*!
 * Will add a row of data to a google spreadsheet
 * @param url The URL of the spreadsheet to add a row to.
 * @param params Keys and values to be
 * @return NSOperationQueue instance wherein you could call [thisInstance cancel] to cancel the request it's doing.
 */
- (NSOperationQueue *) postDataToSpreadsheet:(NSString *)url
                              withDataParams:(NSDictionary *)params
                                withCallback:(void(^)(NSError *error))callBack;

/*!
 * Some Function that will do something.
 * @param url The URL of the spreadsheet to add a row to.
 * @return NSOperationQueue instance wherein you could call [thisInstance cancel] to cancel the request it's doing.
 */
- (NSOperationQueue *) getDataFromSpreadsheet:(NSString *)url
                                 withCallback:(void(^)(NSDictionary *data, NSError *error))callBack;
@end
