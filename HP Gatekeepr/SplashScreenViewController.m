//
//  SplashScreenViewController.m
//  HP Gatekeepr
//
//  Created by Christian Bringino on 8/26/20.
//  Copyright © 2020 Kristine Andrada. All rights reserved.
//

#import "SplashScreenViewController.h"
#import "UserDefaultsHelper.h"

@interface SplashScreenViewController ()

@end

@implementation SplashScreenViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [NSTimer scheduledTimerWithTimeInterval:1 repeats:false block:^(NSTimer * _Nonnull timer) {
        BOOL hasCodeBeenAccepted = [UserDefaultsHelper getCodeAcceptedState];
        if (hasCodeBeenAccepted) {
            [self performSegueWithIdentifier:@"showRooms" sender:nil];
        }else{
            [self performSegueWithIdentifier:@"showHome" sender:nil];
        }
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
