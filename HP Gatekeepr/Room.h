//
//  Room.h
//  HP Gatekeepr
//
//  Created by Christian Bringino on 8/25/20.
//  Copyright © 2020 Kristine Andrada. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Room : NSObject
@property (nonatomic, strong) NSString *room;
@property (nonatomic, strong) NSString *descriptions;

-(instancetype)initWithDataDictionary:(NSDictionary *)dataDict;
@end

NS_ASSUME_NONNULL_END
