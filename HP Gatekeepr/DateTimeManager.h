//
//  DateTimeManager.h
//  SpeakerQuickView
//
//  Created by Aljon Antiola on 6/19/15.
//  Copyright (c) 2015 Training 1. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface DateTimeManager : NSObject

+(BOOL)isNewDate:(NSDate *)newDate newerThanOldDate:(NSDate *)oldDate;
+(NSDate *)getDateFromStr:(NSString *)dateStr;
+(NSString *)getStringFromDate:(NSDate *)date;
+(NSString *)getTimeOnlyFromDate:(NSDate *)date;
+(NSArray *)sortDateFromMostRecent:(NSArray *)dateArray;
+(NSString *)getDateOnlyFromDate:(NSDate *)date;
@end
