//
//  AppDelegate.h
//  EESY Breakout
//
//  Created by Kristine Andrada on 10/13/16.
//  Copyright © 2016 Kristine Andrada. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
-(void)changeRootViewToHome;

@end

