//
//  Constants.h
//  EESY Breakout
//
//  Created by Kristine Andrada on 10/14/16.
//  Copyright © 2016 Kristine Andrada. All rights reserved.
//


#define GGL_SPRDSHT_WRITE_URL_PROFILE @"https://docs.google.com/forms/d/e/1FAIpQLScvCAeOtCx7dCkaZ7aPNEma9NdeuboTddV_k3Xx2b4QCeCP9g/formResponse"
// URL for json version of the spreadsheet

#define GGL_SPRDSHT_DATA_BASE_URL @"https://spreadsheets.google.com/feeds/list"
#define GGL_SPRDSHT_DATA_REST_PARAMS @"public/values?alt=json"

// id of the spreadsheets
#define GGL_SPRDSHT_ID_PROFILE    @"1MOpBod-mqBHRe-n2QCRjext-AXrGVEbhCYTNydcerc8"
#define GGL_SPRDSHT_ID_EVENTS     @"1GSWHHgqTgvYzZjdqWRbV8KBBK8sqqQ6gL812-cLpMg0"
#define GGL_SPRDSHT_ID_SESSION    @"1EydzVNerF5Xc1-XDQqPrKx-QZBJuwnlZrfafMStUkbE"


#define GGL_SPRDSHT_WORKSHEET_DATA             @"1"

// spreadsheet timestamp keys
#define LAST_UPDATED_SCHEDULES_KEY      @"date_schedules"
#define LAST_UPDATED_EVENTS_KEY         @"date_event"

// local keys
#define FILE_SESSION_KEY                @"file_schedules"
#define FILE_EVENT_KEY                  @"file_event"
// keys from breakout session form
#define POST_SESSION_KEY_USER_INFO         @"entry.998189392"
#define POST_SESSION_KEY_SESSION_TOPIC       @"entry.501433723"

#define POST_ATTENDEE_NAME                  @"entry.187976665"
#define POST_ATTENDEE_EMAIL                 @"entry.581131284"
#define POST_ATTENDEE_COMPANY               @"entry.2058859044"
#define POST_ATTENDEE_CONTACT_NUM           @"entry.208623559"
#define POST_ATTENDEE_HEALTH_STAT           @"entry.913912332"
#define POST_ATTENDEE_ROOM                  @"entry.854908237"
#define POST_ATTENDEE_SCANNER_CODE          @"entry.1340456425"
//Color
#define TABLE_HEADER_COLOR  [UIColor colorWithRed:0.93 green:0.93 blue:0.93 alpha:1.0]
#define HEADER_TEXT_COLOR   [UIColor colorWithRed:0.46 green:0.46 blue:0.46 alpha:1.0]
#define PRIMARY_COLOR       [UIColor colorWithRed:0.07 green:0.55 blue:0.82 alpha:1.0];


#define IDIOM    UI_USER_INTERFACE_IDIOM()
#define IPAD     UIUserInterfaceIdiomPad

