//
//  NotificationTableViewCell.h
//  HP Gatekeepr
//
//  Created by Christian Bringino on 8/25/20.
//  Copyright © 2020 Kristine Andrada. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NotificationTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblNotifTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblNotifDesc;
@property (weak, nonatomic) IBOutlet UILabel *lblNotifDate;

@end

NS_ASSUME_NONNULL_END
