//
//  QRCodeGenerator.h
//  Pocket Rocket
//
//  Created by Bryan Posas on 7/27/15.
//  Copyright (c) 2015 ios-training. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface QRCodeGenerator : NSObject

/*!
 * Create a QR Code from a string.
 * @param stringToBeConverted String to be converted to qr code.
 * @param shouldBeCached Option to save the image onto the device.
 * @return QRCode image
 */
+ (UIImage *) generateQRCodeFromString:(NSString *)stringToBeConverted
                        shouldBeCached:(BOOL)shouldBeCached;

/// Directory path to save qr code image
+ (NSString *)pathToSaveQRCodeImage;

/// Directory path to Load qr code image
+ (NSString *)pathToLoadQRCodeImage;

/*!
 * Saves the image into the directory.
 * @param image Image to be saved.
 * @param directoryPath The path where the image will be saved.
 * @return Flag to tell if the saving is successful or not.
 */
+ (BOOL)saveQRCodeImage:(UIImage *)image
            inDirectory:(NSString *)directoryPath;

/// deletes the image from the directory.
+ (void)removeQRImage;

@end
