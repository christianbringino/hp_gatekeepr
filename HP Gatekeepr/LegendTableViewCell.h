//
//  TableViewCell.h
//  HP Scan
//
//  Created by Christian Bringino on 6/26/20.
//  Copyright © 2020 Kristine Andrada. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LegendTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblColorLegend;
@property (weak, nonatomic) IBOutlet UIView *vwMain;

@end

NS_ASSUME_NONNULL_END
