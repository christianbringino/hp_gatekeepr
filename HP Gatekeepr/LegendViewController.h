//
//  LegendViewController.h
//  HP Scan
//
//  Created by Christian Bringino on 6/25/20.
//  Copyright © 2020 Kristine Andrada. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface LegendViewController : PWBaseViewController
@property (nonatomic, assign) NSString *selectedRoom;

@end

NS_ASSUME_NONNULL_END
