//
//  QRCodeGenerator.m
//  Pocket Rocket
//
//  Created by Bryan Posas on 7/27/15.
//  Copyright (c) 2015 ios-training. All rights reserved.
//

#import "QRCodeGenerator.h"

#define QRCODE_FILENAME @"myqrcode.png"

@implementation QRCodeGenerator

#pragma mark - Public Methods
+ (UIImage *) generateQRCodeFromString:(NSString *)stringToBeConverted
                        shouldBeCached:(BOOL)shouldBeCached {
    
    CIFilter *filter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
    
    //    NSLog(@"filterAttributes:%@", filter.attributes);
    
    [filter setDefaults];
    
    NSData *data = [stringToBeConverted dataUsingEncoding:NSUTF8StringEncoding];
    [filter setValue:data forKey:@"inputMessage"];
    
    CIImage *outputImage = [filter outputImage];
    
    CIContext *context = [CIContext contextWithOptions:nil];
    CGImageRef cgImage = [context createCGImage:outputImage
                                       fromRect:[outputImage extent]];
    
    UIImage *image = [UIImage imageWithCGImage:cgImage
                                         scale:1.0
                                   orientation:UIImageOrientationUp];
    
    // Resize without interpolating
    UIImage *resized = [QRCodeGenerator resizeImage:image
                             withQuality:kCGInterpolationNone
                                    rate:5.0];
    
    if ( shouldBeCached ) {
        [QRCodeGenerator saveQRCodeImage:resized inDirectory:[QRCodeGenerator pathToSaveQRCodeImage]];
    }
    
    CGImageRelease(cgImage);
    
    return resized;
    
}

+ (NSURL *)applicationDocumentsDirectory {
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
                                                   inDomains:NSUserDomainMask] lastObject];
}

+ (NSString *)pathToSaveQRCodeImage {
    return [QRCodeGenerator applicationDocumentsDirectory].path;
}

+ (NSString *)pathToLoadQRCodeImage {
    return [NSString stringWithFormat:@"%@/%@", [QRCodeGenerator applicationDocumentsDirectory].path, QRCODE_FILENAME];
    
}

+ (BOOL)saveQRCodeImage:(UIImage *)image
            inDirectory:(NSString *)directoryPath {
    
    NSError *error = nil;
    /* Create folder structure if it doesn't exist */
    if (![[NSFileManager defaultManager] fileExistsAtPath:directoryPath]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:directoryPath withIntermediateDirectories:YES attributes:nil error:&error];
        if (error) {
            NSLog(@"ERROR: %@", error.localizedDescription);
            return NO;
        }
    }
    
    NSString *fullPath = [directoryPath stringByAppendingPathComponent:QRCODE_FILENAME];
    if ( [[NSFileManager defaultManager] fileExistsAtPath:fullPath] ) {
        [[NSFileManager defaultManager] removeItemAtPath:fullPath error:&error];
        
        if (error) {
            NSLog(@"ERROR: %@", error.localizedDescription);
            return NO;
        }
    }
    
    [UIImagePNGRepresentation(image) writeToFile:fullPath atomically:YES];
    
    return YES;
}

#pragma mark - Private Methods

+ (UIImage *)resizeImage:(UIImage *)image
             withQuality:(CGInterpolationQuality)quality
                    rate:(CGFloat)rate
{
    UIImage *resized = nil;
    CGFloat width = image.size.width * rate;
    CGFloat height = image.size.height * rate;
    
    UIGraphicsBeginImageContext(CGSizeMake(width, height));
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetInterpolationQuality(context, quality);
    [image drawInRect:CGRectMake(0, 0, width, height)];
    resized = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return resized;
}


+ (void)removeQRImage{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSString *filePath = [QRCodeGenerator pathToLoadQRCodeImage];
    NSError *error;
    BOOL success = [fileManager removeItemAtPath:filePath error:&error];
    if (success) {
        NSLog(@"Successfully deleted QR image.");
    }
    else
    {
        NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
    }
}
@end
