//
//  ScannedUserDetailsViewController.m
//  HP Scan
//
//  Created by Christian Bringino on 6/30/20.
//  Copyright © 2020 Kristine Andrada. All rights reserved.
//

#import "ScannedUserDetailsViewController.h"
@import SDWebImage;
@import FirebaseDatabase;

@interface ScannedUserDetailsViewController ()

@property (weak, nonatomic) IBOutlet UIView *vwMain;
@property (weak, nonatomic) IBOutlet UIView *vwContent;

@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblTimestamp;

@property (weak, nonatomic) IBOutlet UIImageView *ivPatient;

@property (strong, nonatomic) FIRDatabaseReference *ref;

@end

@implementation ScannedUserDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.ref = [[FIRDatabase database] reference];
    
    [self initViews];
    [self observeUserDetails];
    [self displayUserDetails];
}

- (void)initViews {
    [self addCornerRadiusToView:self.vwContent borderWidth:0 borderColor:UIColor.clearColor cornerRadius: 8];
    [self addCornerRadiusToView:_ivPatient borderWidth:5 borderColor:UIColor.whiteColor cornerRadius: (_ivPatient.frame.size.width / 2)];
}

- (void)addCornerRadiusToView:(UIView *) view
                  borderWidth: (double) borderWidth borderColor: (UIColor *) borderColor cornerRadius: (double) cornerRadius {
    view.layer.cornerRadius = cornerRadius;
    view.layer.borderWidth = borderWidth;
    view.layer.borderColor = borderColor.CGColor;
}

- (void)observeUserDetails {
    [[[self.ref child:@"attendee"] child:self.userId] observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        NSDictionary *dicData = snapshot.value;
        self.name = [dicData objectForKey:@"name"];
        self.imageUrl = [dicData objectForKey:@"imageId"];
        
        [self displayUserDetails];
    } withCancelBlock:^(NSError * _Nonnull error) {
        // error
        [self showAlertWithTitle:@"Error" message:error.localizedDescription];
    }];
}

- (void)displayUserDetails {
    NSURL *url = [NSURL URLWithString: self.imageUrl];
    double timeInterval = [self.timestamp doubleValue];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970: timeInterval];
    
    self.lblName.text = self.name;
    self.lblTimestamp.text = [self convertDateToStringWithFormat:@"MMMM dd, yyyy - h:mm a" date:date];
    
    [self.ivPatient sd_setImageWithURL:url];
    [self setHealthStatusColorIndicator: [self.healthStatus intValue]];
}

-(NSString *) convertDateToStringWithFormat: (NSString *) format date: (NSDate *) date {
    if (date == nil) {
        return @"";
    }
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = format;
    return [dateFormatter stringFromDate: date];
}

-(void) setHealthStatusColorIndicator: (int) status {
    switch (status) {
        case 0:
            self.vwMain.backgroundColor = UIColor.greenColor;
            [self addCornerRadiusToView:_vwMain borderWidth:5 borderColor:UIColor.greenColor cornerRadius:10];
            break;
        case 1:
            self.vwMain.backgroundColor = UIColor.yellowColor;
            [self addCornerRadiusToView:_vwMain borderWidth:5 borderColor:UIColor.yellowColor cornerRadius:10];
            break;
        case 2:
            self.vwMain.backgroundColor = UIColor.redColor;
            [self addCornerRadiusToView:_vwMain borderWidth:5 borderColor:UIColor.redColor cornerRadius:10];
            break;
        default:
            break;
    }
}

- (IBAction)didViewPatientPicture:(id)sender {
    // view patient's picture
}

- (IBAction)didDismiss:(id)sender {
    [self dismissViewControllerAnimated:true completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
