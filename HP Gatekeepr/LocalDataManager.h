//
//  LocalDataManager.h
//  Pocket Rocket
//
//  Created by Aljon Antiola on 6/24/15.
//  Copyright (c) 2015 ios-training. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LocalDataManager : NSObject
//fileName example: schedules
/*!
 * Will save data using NSCoding Encoder
 * @param Data array to be saved
 * @param Name of the file
 */
+ (void) saveData: (NSArray *)dataArray to:(NSString *)fileName;
+ (NSArray *) fetchDatafrom:(NSString *)fileName;
+ (NSString *)getFilePath:(NSString *) fileName;
+(void)removeFilesInDirectory;
+(void)removeFilesAtFilePath:(NSString *)filename;
@end
