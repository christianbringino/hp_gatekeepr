//
//  ProjectSettings.m
//  HP Scan
//
//  Created by Christian Bringino on 6/26/20.
//  Copyright © 2020 Kristine Andrada. All rights reserved.
//

#import "ColorLegend.h"

@implementation ColorLegend

- (instancetype)initWithDataDictionary:(NSDictionary *)dataDict {
    self.name = [dataDict objectForKey:@"name"];
    self.colorHex = [dataDict objectForKey:@"colorHex"];
    return self;
}

@end
