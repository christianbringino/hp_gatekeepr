//
//  LocalDataManager.m
//  Pocket Rocket
//
//  Created by Aljon Antiola on 6/24/15.
//  Copyright (c) 2015 ios-training. All rights reserved.
//

#import "LocalDataManager.h"

@implementation LocalDataManager

+ (void) saveData: (NSArray *)dataArray to:(NSString *)fileName{
    if (dataArray != nil && fileName != nil) {
        [NSKeyedArchiver archiveRootObject:dataArray toFile:[self getFilePath:fileName]];
    }
}

+ (NSArray *) fetchDatafrom:(NSString *)fileName{
    
    NSArray *array = [NSKeyedUnarchiver unarchiveObjectWithFile:[self getFilePath:fileName]];
    return array;
    
}


+ (NSString *)getFilePath:(NSString *) fileName{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    documentsDirectory = [documentsDirectory stringByAppendingPathComponent:@"Private Documents"];
    
    NSError *error;
    [[NSFileManager defaultManager] createDirectoryAtPath:documentsDirectory withIntermediateDirectories:YES attributes:nil error:&error];
    
    return [documentsDirectory stringByAppendingPathComponent:fileName];
    
}
+(void)removeFilesInDirectory{
    
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    documentsDirectory = [documentsDirectory stringByAppendingPathComponent:@"Private Documents"];
    
    NSArray *fileArray = [fileMgr contentsOfDirectoryAtPath:documentsDirectory error:nil];
    for (NSString *filename in fileArray)  {
        [fileMgr removeItemAtPath:[documentsDirectory stringByAppendingPathComponent:filename] error:NULL];
    }
}
+(void)removeFilesAtFilePath:(NSString *)filename{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    documentsDirectory = [documentsDirectory stringByAppendingPathComponent:@"Private Documents"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
//    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:filename];
    NSError *error;
    BOOL success = [fileManager removeItemAtPath:filePath error:&error];
    if (success) {
        NSLog(@"Success");
    }
    else
    {
        NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
    }
}


@end
