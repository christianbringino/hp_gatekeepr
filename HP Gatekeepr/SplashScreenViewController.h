//
//  SplashScreenViewController.h
//  HP Gatekeepr
//
//  Created by Christian Bringino on 8/26/20.
//  Copyright © 2020 Kristine Andrada. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface SplashScreenViewController : PWBaseViewController

@end

NS_ASSUME_NONNULL_END
