//
//  LegendViewController.m
//  HP Scan
//
//  Created by Christian Bringino on 6/25/20.
//  Copyright © 2020 Kristine Andrada. All rights reserved.
//

#import "LegendViewController.h"
#import "ColorLegend.h"
#import "LegendTableViewCell.h"
#import "LocalDataManager.h"
#import "AppDelegate.h"
#import "QRCodeReaderViewController.h"
#import "WebServicesManager.h"
#import "UserDefaultsHelper.h"
#import "ScannedUserDetailsViewController.h"
@import FirebaseDatabase;
@import FirebaseFunctions;

@interface LegendViewController ()<UITableViewDelegate, UITableViewDataSource, QRCodeReaderDelegate>{
    NSMutableArray *colorLegends;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) FIRDatabaseReference *ref;
@property(strong, nonatomic) FIRFunctions *functions;
@property (weak, nonatomic) IBOutlet UIButton *btnScan;

@end

@implementation LegendViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.ref = [[FIRDatabase database] reference];
    self.functions = [FIRFunctions functions];
    colorLegends = [[NSMutableArray alloc] init];
    
    [self createLoadingScreen];
    [self fetchColorLegends];
    
    NSString *buttonName = [NSString stringWithFormat:@"%@ : %@", @"SCAN", _selectedRoom];
    
    [_btnScan setTitle:buttonName forState:UIControlStateNormal];
}

- (void)fetchColorLegends {
    [self loadSpinner];
    [[[self.ref child:@"projectSettings"] child:@"colorLegend"] observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        [self stopSpinner];
        
        [self convertToColorLegendObjThenAddToArray:snapshot.value];
        NSLog(@"%@", snapshot.value);
    } withCancelBlock:^(NSError * _Nonnull error) {
        [self stopSpinner];
        // error
        [self showAlertWithTitle:@"Error" message:error.localizedDescription];
        NSLog(@"%@", error.localizedDescription);
    }];
}

- (void)convertToColorLegendObjThenAddToArray: (NSArray*) dicArray {
    [colorLegends removeAllObjects];
    
    for (int i = 0; i < dicArray.count; i++) {
        NSDictionary *dic = dicArray[i];
        ColorLegend *colorLegend = [[ColorLegend alloc] initWithDataDictionary:dic];
        
        [colorLegends addObject:colorLegend];
        
        NSLog(@"%@", colorLegend);
    }
    
    [self.tableView reloadData];
}

- (IBAction)didOpenScanner:(id)sender {
    [self scanQRCodeWithSession];
//    [self postQRCodeInfoToSpreadsheet:nil];
}

-(void)scanQRCodeWithSession {
    if ([QRCodeReader supportsMetadataObjectTypes:@[AVMetadataObjectTypeQRCode]]) {
        static QRCodeReaderViewController *vc = nil;
        static dispatch_once_t onceToken;
        
        dispatch_once(&onceToken, ^{
            QRCodeReader *reader = [QRCodeReader readerWithMetadataObjectTypes:@[AVMetadataObjectTypeQRCode]];
            vc = [QRCodeReaderViewController readerWithCancelButtonTitle:@"Cancel" codeReader:reader startScanningAtLoad:YES showSwitchCameraButton:NO showTorchButton:YES];
            vc.modalPresentationStyle = UIModalPresentationFormSheet;
        });
        vc.delegate = self;
        
        [vc setCompletionWithBlock:^(NSString *resultAsString) {
            // NSLog(@"Completion with result: %@", resultAsString);
        }];
        
        [self presentViewController:vc animated:YES completion:NULL];
    }
    else {
        [self showAlertWithTitle:@"Error" message:@"Reader not supported by the current device"];
    }
    
    
}

- (void)reader:(QRCodeReaderViewController *)reader didScanResult:(NSString *)result {
    NSString *qrCodeScannedString = result;
    
    [self dismissViewControllerAnimated:YES completion:^{
        if ([qrCodeScannedString length] != 0) {
            [self fetchAttendeeData:qrCodeScannedString];
        }
    }];
}

- (void)fetchAttendeeData:(NSString *) userId {
    NSDictionary *params = @{@"userId": userId};
    
    [self loadSpinner];
    [[self.functions HTTPSCallableWithName:@"fetchAttendeeData"] callWithObject:params completion:^(FIRHTTPSCallableResult * _Nullable result, NSError * _Nullable error) {
        [self stopSpinner];
        
        if (error != nil) {
            if (error.code == 13) {
                [self showAlertWithTitle:@"Error" message:@"Patient not found"];
            } else {
                [self showAlertWithTitle:@"Error" message:error.localizedDescription];
            }
            return;
        }
        
        NSDictionary *dic = result.data;
        NSDictionary *dicData = [dic objectForKey:@"data"];
        NSString *strError = [dic objectForKey:@"error"];
        
        if (strError != nil) {
            [self showAlertWithTitle:@"Error" message:strError];
            return;
        }
        
        [self showPatientDetails:dicData];
        [self postQRCodeInfoToSpreadsheet:dicData];
    }];
}

-(void)showPatientDetails: (NSDictionary *) dicData {
    NSDictionary *healthCheckData = [dicData objectForKey:@"healthCheckData"];
    NSString *userId = [dicData objectForKey:@"userId"];
    NSString *name = [dicData objectForKey:@"name"];
    NSString *imageId = [dicData objectForKey:@"imageId"];
    NSNumber *healthStatus = (NSNumber *) [healthCheckData objectForKey:@"status"];
    NSNumber *healthStatusTimeStamp = (NSNumber *) [healthCheckData objectForKey:@"timestamp"];
    
    ScannedUserDetailsViewController *sulvc = [self.storyboard instantiateViewControllerWithIdentifier:@"ScannedUserDetailsViewController"];
    sulvc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    sulvc.userId = userId;
    sulvc.name = name;
    sulvc.imageUrl = imageId;
    sulvc.healthStatus = healthStatus;
    sulvc.timestamp = healthStatusTimeStamp;
    
    [self presentViewController:sulvc animated:true completion:nil];
}

-(void)postQRCodeInfoToSpreadsheet: (NSDictionary *) dicData {
    NSString *scannerName = [UserDefaultsHelper getScannerName];
    if ([scannerName length]==0) {
        scannerName = @"None";
    }
//    NSString *points = _txtPoints.text;
//    if ([points length] == 0) {
//        points = @"0";
//    }
    [self loadSpinner];
//    [self dismissKeyboard];
    NSDictionary *healthCheckData = [dicData objectForKey:@"healthCheckData"];
    
    NSString *url = @"https://docs.google.com/forms/d/e/1FAIpQLSdw-pkuCHyT6pEtK5PsJsxSwY9r6pUMkpKCE-p7wyWSjYbFGw/formResponse";
    NSString *name = [dicData objectForKey:@"name"];
    NSString *email = [dicData objectForKey:@"email"];
    NSString *contactNumber = [dicData objectForKey:@"contactNumber"];
    NSString *company = [dicData objectForKey:@"company"];
    NSString *scannerGuardCode = [UserDefaultsHelper getScannerGuardCode];
    NSString *healthStatus = [healthCheckData objectForKey:@"status"];
    
    
    NSDictionary *params = @{
                             POST_ATTENDEE_NAME : name
                             ,POST_ATTENDEE_EMAIL: email
                             ,POST_ATTENDEE_CONTACT_NUM : contactNumber
                             ,POST_ATTENDEE_COMPANY : company
                             ,POST_ATTENDEE_SCANNER_CODE : scannerGuardCode
                             ,POST_ATTENDEE_HEALTH_STAT : healthStatus
                             ,POST_ATTENDEE_ROOM : _selectedRoom
                             };
    
//    #define POST_ATTENDEE_NAME                  @"entry.187976665"
//    #define POST_ATTENDEE_EMAIL                 @"entry.581131284"
//    #define POST_ATTENDEE_COMPANY               @"entry.2058859044"
//    #define POST_ATTENDEE_CONTACT_NUM           @"entry.208623559"
//    #define POST_ATTENDEE_HEALTH_STAT           @"entry.913912332"
//    #define POST_ATTENDEE_ROOM                  @"entry.854908237"
//    #define POST_ATTENDEE_SCANNER_CODE          @"entry.1340456425"
    
    [[WebServicesManager sharedManager] postDataToSpreadsheet:url withDataParams:params withCallback:^(NSError *error) {
        [self stopSpinner];
        if (!error) {
            NSLog(@"%@", @"Success!");
        }else{
            if (![Reachability isInternetAvailable]) {
//                [self showErrorMessage: @"The internet connection appears to be offline." andTitle:@"Error"];
                [self showAlertWithTitle:@"Error" message:@"The internet connection appears to be offline."];
            }else{
//                 [self showErrorMessage: @"Registration Failed. Please try again." andTitle:@"Error"];
                [self showAlertWithTitle:@"Error" message:@"Registration Failed. Please try again."];
            }
        }
    }];
}

- (void)readerDidCancel:(QRCodeReaderViewController *)reader {
    [reader dismissViewControllerAnimated:true completion:nil];
}

- (IBAction)didPopback:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    LegendTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LegendTableViewCell" forIndexPath:indexPath];
    ColorLegend *colorLegend = colorLegends[[indexPath row]];
    
    cell.lblColorLegend.text = colorLegend.name;
    cell.vwMain.backgroundColor = [self colorWithHexString:colorLegend.colorHex];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 65;
}

- (UIColor *)colorWithHexString:(NSString *)stringToConvert {
    NSString *noHashString = [stringToConvert stringByReplacingOccurrencesOfString:@"#" withString:@""]; // remove the #
    NSScanner *scanner = [NSScanner scannerWithString:noHashString];
    [scanner setCharactersToBeSkipped:[NSCharacterSet symbolCharacterSet]]; // remove + and $

    unsigned hex;
    if (![scanner scanHexInt:&hex]) return nil;
    int r = (hex >> 16) & 0xFF;
    int g = (hex >> 8) & 0xFF;
    int b = (hex) & 0xFF;

    return [UIColor colorWithRed:r / 255.0f green:g / 255.0f blue:b / 255.0f alpha:1.0f];
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return colorLegends.count;
}

@end
