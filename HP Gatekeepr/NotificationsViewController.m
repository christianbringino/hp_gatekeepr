//
//  NotificationsViewController.m
//  HP Gatekeepr
//
//  Created by Christian Bringino on 8/25/20.
//  Copyright © 2020 Kristine Andrada. All rights reserved.
//

#import "NotificationsViewController.h"
#import "NotificationTableViewCell.h"
#import "Notifications.h"

@interface NotificationsViewController ()<UITableViewDelegate, UITableViewDataSource>

@end

@implementation NotificationsViewController {
    NSMutableArray *arrNotifs;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.ref = [[FIRDatabase database] reference];
    arrNotifs = [[NSMutableArray alloc] init];
    
    [self createLoadingScreen];
    [self fetchNotifications];
}

- (void)fetchNotifications {
    [self loadSpinner];
    [[self.ref child:@"notifications"] observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        [self stopSpinner];
        
        [self convertToNotificationsObjThenAddToArray:snapshot.value];
        NSLog(@"%@", snapshot.value);
    } withCancelBlock:^(NSError * _Nonnull error) {
        [self showAlertWithTitle:@"Error" message:error.localizedDescription];
    }];
}

- (void)convertToNotificationsObjThenAddToArray: (NSDictionary*) dicNotifs {
    [arrNotifs removeAllObjects];
    
    for (NSDictionary *dic in [dicNotifs allValues]) {
        Notifications *notif = [[Notifications alloc] initWithDataDictionary:dic];
        
        [arrNotifs addObject:notif];
    }
    
    NSSortDescriptor *dateSorter = [[NSSortDescriptor alloc] initWithKey:@"notifTimestamp" ascending:NO];
    [arrNotifs sortUsingDescriptors:[NSArray arrayWithObject:dateSorter]];
    
    [self.tableView reloadData];
}

- (IBAction)didPopback:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrNotifs.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NotificationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NotificationTableViewCell" forIndexPath:indexPath];
    Notifications *notif = arrNotifs[indexPath.row];
    
    cell.lblNotifTitle.text = notif.notifTitle;
    cell.lblNotifDesc.text = notif.notifMessage;
    cell.lblNotifDate.text = notif.notifTimestamp;
    
    return cell;
}



@end
