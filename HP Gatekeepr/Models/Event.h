//
//  Event.h
//  EESY Breakout
//
//  Created by Kristine Andrada on 21/05/2017.
//  Copyright © 2017 Kristine Andrada. All rights reserved.
//

#import <Foundation/Foundation.h>

#define JSONDATA_EVENT_ID            @"gsx$id"
#define JSONDATA_EVENT_NAME          @"gsx$eventname"
#define JSONDATA_EVENT_CODE         @"gsx$eventcode"
#define JSONDATA_EVENT_URL          @"gsx$eventurl"

@interface Event : NSObject
@property (nonatomic, strong) NSString *identifier;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *code;
@property (nonatomic, strong) NSString *url;

-(instancetype)initWithDataDictionary:(NSDictionary *)dataDict;
@end
