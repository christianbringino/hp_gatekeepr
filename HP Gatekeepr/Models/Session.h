//
//  Session.h
//  EESY Breakout
//
//  Created by Kristine Andrada on 10/17/16.
//  Copyright © 2016 Kristine Andrada. All rights reserved.
//

#import <Foundation/Foundation.h>

#define JSONDATA_SESSION_KEY_SESSION             @"gsx$session"
#define JSONDATA_SESSION_KEY_SESSION_TYPE        @"gsx$sessiontype"
#define JSONDATA_SESSION_KEY_START_TIME          @"gsx$start"
#define JSONDATA_SESSION_KEY_END_TIME            @"gsx$end"
#define JSONDATA_SESSION_KEY_FORM_URL            @"gsx$formurl"

@interface Session : NSObject
@property (nonatomic, strong) NSString *session;
@property (nonatomic, strong) NSString *start;
@property (nonatomic, strong) NSString *end;
@property (nonatomic, strong) NSString *formURL;
@property (nonatomic, strong) NSString *sessionType;

-(instancetype)initWithDataDictionary:(NSDictionary *)dataDict;
@end
