//
//  Event.m
//  EESY Breakout
//
//  Created by Kristine Andrada on 21/05/2017.
//  Copyright © 2017 Kristine Andrada. All rights reserved.
//

#import "Event.h"

@implementation Event

-(instancetype)initWithDataDictionary:(NSDictionary *)dataDict{
    self = [super init];
    if (self) {
        _identifier = [dataDict objectForKey:JSONDATA_EVENT_ID];
        _name   = [dataDict objectForKey:JSONDATA_EVENT_NAME];
        _code   = [dataDict objectForKey:JSONDATA_EVENT_CODE];
        _url    = [dataDict objectForKey:JSONDATA_EVENT_URL];
        
    }
    return self;
}

#pragma mark - NSCoding
#define idKey           @"idKey"
#define nameKey          @"nameKey"
#define codeKey         @"codeKey"
#define urlKey           @"urlKey"

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:_identifier             forKey:idKey];
    [encoder encodeObject:_name                 forKey:nameKey];
    [encoder encodeObject:_code               forKey:codeKey];
    [encoder encodeObject:_url                 forKey:urlKey];
  
}
- (id)initWithCoder:(NSCoder *)decoder {
    _identifier          = [decoder decodeObjectForKey:idKey];
    _name            = [decoder decodeObjectForKey:nameKey];
    _code            = [decoder decodeObjectForKey:codeKey];
    _url             = [decoder decodeObjectForKey:urlKey];
   
    return self;
}
@end
