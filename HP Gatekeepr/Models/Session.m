//
//  Session.m
//  EESY Breakout
//
//  Created by Kristine Andrada on 10/17/16.
//  Copyright © 2016 Kristine Andrada. All rights reserved.
//

#import "Session.h"

@implementation Session

-(instancetype)initWithDataDictionary:(NSDictionary *)dataDict{
    self = [super init];
    if (self) {
        _session = [dataDict objectForKey:JSONDATA_SESSION_KEY_SESSION];
        _sessionType  = [dataDict objectForKey:JSONDATA_SESSION_KEY_SESSION_TYPE];
        _start   = [dataDict objectForKey:JSONDATA_SESSION_KEY_START_TIME];
        _end     = [dataDict objectForKey:JSONDATA_SESSION_KEY_END_TIME];
        _formURL = [dataDict objectForKey:JSONDATA_SESSION_KEY_FORM_URL];
        
    }
    return self;
}

#pragma mark - NSCoding
#define sessionKey              @"sessionKey"
#define sessionTypeKey          @"sessionTypeKey"
#define startKey                @"startKey"
#define endKey                  @"endKey"
#define formURLKey              @"formURLKey"

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:_session             forKey:sessionKey];
    [encoder encodeObject:_sessionType         forKey:sessionTypeKey];
    [encoder encodeObject:_start               forKey:startKey];
    [encoder encodeObject:_end                 forKey:endKey];
    [encoder encodeObject:_formURL             forKey:formURLKey];
}
- (id)initWithCoder:(NSCoder *)decoder {
    _session         =          [decoder decodeObjectForKey:sessionKey];
    _sessionType     =          [decoder decodeObjectForKey:sessionTypeKey];
    _start           =          [decoder decodeObjectForKey:startKey];
    _end             =          [decoder decodeObjectForKey:endKey];
    _formURL         =          [decoder decodeObjectForKey:formURLKey];
    return self;
}
@end
