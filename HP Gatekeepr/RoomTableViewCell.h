//
//  RoomTableViewCell.h
//  HP Gatekeepr
//
//  Created by Christian Bringino on 8/25/20.
//  Copyright © 2020 Kristine Andrada. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RoomTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblRoom;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;

@end

NS_ASSUME_NONNULL_END
