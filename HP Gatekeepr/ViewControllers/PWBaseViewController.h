//
//  PWBaseViewController.h
//  EESY Breakout
//
//  Created by Kristine Andrada on 10/17/16.
//  Copyright © 2016 Kristine Andrada. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"
#import "Constants.h"

@interface PWBaseViewController : UIViewController
-(void)setBackBarButtonTitleTo:(NSString *)title;
- (void)setUpNavBarFontAndBG;
-(void)createLoadingScreen;
- (void)loadSpinner;
- (void)stopSpinner;
- (void)showAlertWithTitle: (NSString *)title message: (NSString *) message;
@end
