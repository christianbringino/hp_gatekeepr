//
//  PointsViewController.h
//  EESY Breakout
//
//  Created by Kristine Andrada on 5/2/18.
//  Copyright © 2018 Kristine Andrada. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PWBaseViewController.h"
#import "SessionListDelegate.h"

@interface PointsViewController : PWBaseViewController<UITextFieldDelegate, SessionListDelegate>
@property (nonatomic, strong) NSArray *contactArr;
@property (nonatomic, strong) NSString *attendanceUrl;
@property (weak, nonatomic) id<SessionListDelegate>delegate;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblContact;
@property (weak, nonatomic) IBOutlet UILabel *lblCompany;
@property (weak, nonatomic) IBOutlet UILabel *lblJobTitle;




@end
