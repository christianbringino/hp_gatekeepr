//
//  PWBaseViewController.m
//  EESY Breakout
//
//  Created by Kristine Andrada on 10/17/16.
//  Copyright © 2016 Kristine Andrada. All rights reserved.
//

#import "PWBaseViewController.h"


@interface PWBaseViewController ()

@end

@implementation PWBaseViewController{
    UIWindow *window;
    UIActivityIndicatorView *spinner;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
- (void)setUpNavBarFontAndBG{
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setTitleTextAttributes: @{
                                                                       NSForegroundColorAttributeName: [UIColor whiteColor],
                                                                       NSFontAttributeName: [UIFont systemFontOfSize:20.0f]
                                                                       }];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor = PRIMARY_COLOR;
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

-(void)setBackBarButtonTitleTo:(NSString *)title{
    UIBarButtonItem *newBackButton =
    [[UIBarButtonItem alloc] initWithTitle:title
                                     style:UIBarButtonItemStylePlain
                                    target:nil
                                    action:nil];
    [[self navigationItem] setBackBarButtonItem:newBackButton];
}
-(void)createLoadingScreen{
    window = [UIApplication sharedApplication].keyWindow;
    spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    spinner.frame = CGRectMake(0.0, 0.0, 40.0, 40.0);
    spinner.center = window.center;
    // spinner.backgroundColor = [UIColor colorWithWhite:0. alpha:0.6];
    spinner.layer.cornerRadius = 5;
}
- (void)loadSpinner{
    [self.view addSubview:spinner];
    [spinner startAnimating];
}

- (void)stopSpinner{
    [spinner removeFromSuperview];
    [spinner stopAnimating];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showAlertWithTitle:(NSString *)title message:(NSString *)message {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:alert animated:true completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
