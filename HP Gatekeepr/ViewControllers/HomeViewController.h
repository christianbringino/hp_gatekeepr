//
//  HomeViewController.h
//  EESY Breakout
//
//  Created by Kristine Andrada on 10/17/16.
//  Copyright © 2016 Kristine Andrada. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PWBaseViewController.h"

@interface HomeViewController : PWBaseViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *txtEventCode;

@end
