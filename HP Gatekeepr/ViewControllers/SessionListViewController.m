//
//  SessionListViewController.m
//  EESY Breakout
//
//  Created by Kristine Andrada on 10/17/16.
//  Copyright © 2016 Kristine Andrada. All rights reserved.
//

#import "SessionListViewController.h"
#import "Constants.h"
#import "WebServicesManager.h"
#import "LocalDataManager.h"
#import "DateTimeManager.h"
#import "PWGoogleSpreadsheetParser.h"
#import "OrderedDictionary.h"
#import "Session.h"
#import "SessionListTableViewCell.h"
#import "UserDefaultsHelper.h"
#import "PointsViewController.h"
#import "AppDelegate.h"
#import "StringFormatter.h"

#define ALERT_TAG_ERROR   1
#define ALERT_TAG_SUCCESS 2

@interface SessionListViewController ()
@property (weak, nonatomic) IBOutlet UIView *viewSuccessfullyRegistered;
@property (weak, nonatomic) IBOutlet UILabel *lblSuccessfullyRegistered;

@end

@implementation SessionListViewController{
    LocalDataManager *localDataManager;
    NSArray         *arraySchedules;
    MutableOrderedDictionary * sessionDictionary;
    NSArray *dictionaryKeysArray;
    SessionListTableViewCell *cell;
    BOOL isBoxSelected;
    Session *sessionToScan;
    UITableViewController *tableViewController;
    NSMutableArray *headerTitleArr;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpNavBarFontAndBG];
    [self setUpNavBar];
    // self.navigationController.navigationBar.
    self.navigationItem.title = @"Select a Session";
    
    self.sessionTableView.dataSource = self;
    self.sessionTableView.delegate = self;
    self.sessionTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    arraySchedules = [[NSArray alloc]init];
    dictionaryKeysArray = [[NSArray alloc]init];
    localDataManager = [[LocalDataManager alloc]init];
    [self downloadSchedule];
    arraySchedules = [LocalDataManager fetchDatafrom:FILE_SESSION_KEY];
    [self fetchSessionDetails];
    self.sessionTableView.rowHeight = UITableViewAutomaticDimension;
    self.sessionTableView.estimatedRowHeight = 150;
    isBoxSelected = NO;
    self.viewSuccessfullyRegistered.hidden=YES;
    [self addRefreshToTable];
    [self fetchRooms];
    
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.navigationItem setHidesBackButton:YES];
}

#pragma mark - Private Methods
-(void)setUpNavBar{
    UIView *navView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 80.0f, 30.0f)];
    //SHARE BUTTON
    UIButton *btnSettings = [UIButton buttonWithType:UIButtonTypeSystem];
    [btnSettings setFrame:CGRectMake(50.0f, 1.0f, 30.0f, 30.0f)];
    //  [btn2 setTitle:@"Share" forState:UIControlStateNormal];
    [btnSettings setBackgroundImage:[UIImage imageNamed:@"app_logout"] forState:UIControlStateNormal];
    [btnSettings addTarget:self action:@selector(createLogoutAlert) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:btnSettings];
    
    UIBarButtonItem *rightBtn = [[UIBarButtonItem alloc] initWithCustomView:navView];
    [self.navigationItem setRightBarButtonItem:rightBtn];
    
}
#pragma mark
-(void)createLogoutAlert{
    UIAlertController * logoutAlert =   [UIAlertController
                                         alertControllerWithTitle:@"Log Out"
                                         message:@"This will delete all the data stored in the app. Are you sure you want to log out?"
                                         preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* logoutAction = [UIAlertAction
                             actionWithTitle:@"LOGOUT"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [self logoutUser];
                             }];
    [logoutAlert addAction:logoutAction]; // add action to uialertcontroller
    UIAlertAction*cancelAction = [UIAlertAction
                            actionWithTitle:@"CANCEL"
                            style:UIAlertActionStyleCancel
                            handler:^(UIAlertAction * action)
                            {
                            }];
    [logoutAlert addAction:cancelAction]; // add action to uialertcontroller
    [self presentViewController:logoutAlert animated:YES completion:nil];
}
-(void)logoutUser{
    
    [LocalDataManager removeFilesInDirectory];
    NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] changeRootViewToHome];
}
-(void)goToPointsController:(NSArray *)contactArr{
    PointsViewController *pvc = [self.storyboard instantiateViewControllerWithIdentifier:@"PointsViewController"];
    pvc.contactArr = contactArr;
    pvc.attendanceUrl = sessionToScan.formURL;
    pvc.delegate = self;
    UIBarButtonItem *newBackButton =
    [[UIBarButtonItem alloc] initWithTitle:@"Back"
                                     style:UIBarButtonItemStyleBordered
                                    target:nil
                                    action:nil];
    [[self navigationItem] setBackBarButtonItem:newBackButton];
    [self.navigationController pushViewController:pvc animated:YES];
    
}
-(void)addRefreshToTable{
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(downloadSchedule) forControlEvents:UIControlEventValueChanged];
    //[self.mytable addSubview:refreshControl];
    tableViewController = [[UITableViewController alloc] init];
    tableViewController.tableView = self.sessionTableView;
    tableViewController.refreshControl = refreshControl;
}

- (void) fetchRooms {
    NSString *url = @"https://spreadsheets.google.com/feeds/list/1ZGcXei_aeFc2RxIO7W25CawIJDE82azuLhVBnLaaNhg/1/public/values?alt=json";
    
    [[WebServicesManager sharedManager] getDataFromSpreadsheet:url withCallback:^(NSDictionary *data, NSError *error) {
        //      [spinner stopAnimating];
        //      self.navigationItem.rightBarButtonItem = refresh;
        if ((error == nil) && (data != nil)) {
            NSDictionary *feed = [data objectForKey:@"feed"];
            NSDictionary *updated = [feed objectForKey:@"updated"];
//            NSDate *newDate = [updated objectForKey:@"$t"];
            
            NSArray<NSDictionary *> *arrDic = [feed objectForKey:@"entry"];
            
            
            NSLog(@"%@", @"yo");
            
//            NSArray<NSDictionary *> *arrDic = [feed objectForKey:@"entry"];
//
//            for (NSDictionary *dic in arrDic) {
//                NSDictionary *dicCode = [dic objectForKey:@"gsx$code"];
//                NSString *strCode = [dicCode objectForKey:@"$t"];
//
//                if (strCode == _txtEventCode.text) {
//                    [self performSegueWithIdentifier:@"showSessionList" sender:nil];
//                    break;
//                }
//            }
            
//            NSDate *oldDate = (NSDate *)[[NSUserDefaults standardUserDefaults] objectForKey:LAST_UPDATED_EVENTS_KEY];
//            NSArray *temp = [LocalDataManager fetchDatafrom:FILE_EVENT_KEY];
//            if (oldDate && temp) {
//                if ([DateTimeManager isNewDate:newDate newerThanOldDate:oldDate]) {
//                    [LocalDataManager removeFilesAtFilePath:FILE_EVENT_KEY];
//                    [LocalDataManager saveData:[PWGoogleSpreadsheetParser parseResponseObject:data] to:FILE_EVENT_KEY];
//                    [[NSUserDefaults standardUserDefaults] setObject:newDate forKey:LAST_UPDATED_EVENTS_KEY];
//                }
//            }
//            else{
//                [LocalDataManager saveData:[PWGoogleSpreadsheetParser parseResponseObject:data] to:FILE_EVENT_KEY];
//                [[NSUserDefaults standardUserDefaults] setObject:newDate forKey:LAST_UPDATED_EVENTS_KEY];
//
//            }
        }
        
        else {
            
        }
        
    }];
}

- (void)downloadSchedule{
    NSString *eventUrl = [UserDefaultsHelper getEventInfoUrl];
    NSString *url = [NSString stringWithFormat:@"https://spreadsheets.google.com/feeds/list/%@/1/public/values?alt=json", eventUrl];
    
    [[WebServicesManager sharedManager] getDataFromSpreadsheet:url withCallback:^(NSDictionary *data, NSError *error) {
        [tableViewController.refreshControl endRefreshing];
        
        if ((error == nil) && (data != nil)) {
            NSDictionary *feed = [data objectForKey:@"feed"];
            NSDictionary *updated = [feed objectForKey:@"updated"];
            NSDate *newDate = [updated objectForKey:@"$t"];
            
            NSDate *oldDate = (NSDate *)[[NSUserDefaults standardUserDefaults] objectForKey:LAST_UPDATED_SCHEDULES_KEY];
            NSArray *temp = [LocalDataManager fetchDatafrom:FILE_SESSION_KEY];
            if (oldDate && temp) {
                if ([DateTimeManager isNewDate:newDate newerThanOldDate:oldDate]) {
                    [LocalDataManager removeFilesAtFilePath:FILE_SESSION_KEY];
                    [LocalDataManager saveData:[PWGoogleSpreadsheetParser parseResponseObject:data] to:FILE_SESSION_KEY];
                    [[NSUserDefaults standardUserDefaults] setObject:newDate forKey:LAST_UPDATED_SCHEDULES_KEY];
                    arraySchedules = [LocalDataManager fetchDatafrom:FILE_SESSION_KEY];
                    [self fetchSessionDetails];
                    
                }else{
                    arraySchedules = [LocalDataManager fetchDatafrom:FILE_SESSION_KEY];
                    [self fetchSessionDetails];
                }
                
            }
            else{
                [LocalDataManager saveData:[PWGoogleSpreadsheetParser parseResponseObject:data] to:FILE_SESSION_KEY];
                [[NSUserDefaults standardUserDefaults] setObject:newDate forKey:LAST_UPDATED_SCHEDULES_KEY];
                arraySchedules = [LocalDataManager fetchDatafrom:FILE_SESSION_KEY];
                [self fetchSessionDetails];
                
            }
        }
        
        else {
            if(![Reachability isInternetAvailable]){
                [self displayNoNetWorkAlert];
            }
            //  [self.toast showToastWithMessage:error.localizedDescription];
        }
        
    }];
}

-(void)fetchSessionDetails{
    NSMutableArray *sessionArr = [[NSMutableArray alloc]init];
    sessionDictionary= [[MutableOrderedDictionary alloc]init];
    for (NSDictionary *item in arraySchedules) {
        Session *session = [[Session alloc]initWithDataDictionary:item];
        [sessionArr addObject:session];
    }
    [self filterArrayBaseOnTime:sessionArr];
}
-(void)filterArrayBaseOnTime:(NSMutableArray *)newArrayMutable{
      headerTitleArr = [[NSMutableArray alloc]init];
    for (Session *session in newArrayMutable) {
        
        NSDate *startDate = [DateTimeManager getDateFromStr:session.start];
        NSString *dateStr = [DateTimeManager getStringFromDate:startDate];
        NSString *key = dateStr;
//        NSString *startTimeStr = [DateTimeManager getTimeOnlyFromDate:startDate];
//        NSString *key = startTimeStr;
        NSDate *endDate = [DateTimeManager getDateFromStr:session.end];
        NSString *dateOnlyStr = [DateTimeManager getDateOnlyFromDate:startDate];
        NSString *formattedDate = [StringFormatter changeDateStringFormat:dateOnlyStr];
        NSString *startTime = [DateTimeManager getTimeOnlyFromDate:startDate];
        NSString *endTime = [DateTimeManager getTimeOnlyFromDate:endDate];
        NSString *headerTitle;
        if ([endTime length]==0) {
            headerTitle =  [NSString stringWithFormat:@"%@, %@",formattedDate, startTime];
        }else{
            headerTitle =  [NSString stringWithFormat:@"%@, %@ - %@",formattedDate, startTime, endTime];
        }
        [headerTitleArr addObject:headerTitle];
        
        NSMutableArray *tempArray = sessionDictionary[key];
        if (!tempArray) {
            tempArray = [NSMutableArray array];
        }
        [tempArray addObject:session];
        sessionDictionary[key] = tempArray;
    }
    dictionaryKeysArray = [sessionDictionary allKeys];
    dictionaryKeysArray = [DateTimeManager sortDateFromMostRecent:dictionaryKeysArray];
    [self.sessionTableView reloadData];
}

-(void)displayNoNetWorkAlert{
    UIAlertController * chooseSessionAlert =   [UIAlertController
                                                alertControllerWithTitle:@"No Network Connection"
                                                message:@"The internet connection appears to be offline."
                                                preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             //Do some thing here
                             // [view dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    [chooseSessionAlert addAction:ok]; // add action to uialertcontroller
    
    
    [self presentViewController:chooseSessionAlert animated:YES completion:nil];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return dictionaryKeysArray.count;
    
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *sectionTitle = [dictionaryKeysArray objectAtIndex:section];
    NSArray *sessionArr = [sessionDictionary objectForKey:sectionTitle];
    Session *session = [sessionArr objectAtIndex:0];
    
    NSDate *startDate = [DateTimeManager getDateFromStr:session.start];
    NSDate *endDate = [DateTimeManager getDateFromStr:session.end];
    NSString *dateOnlyStr = [DateTimeManager getDateOnlyFromDate:startDate];
    NSString *formattedDate = [StringFormatter changeDateStringFormat:dateOnlyStr];
    NSString *startTime = [DateTimeManager getTimeOnlyFromDate:startDate];
    NSString *endTime = [DateTimeManager getTimeOnlyFromDate:endDate];
    NSString *headerTitle;
    if ([endTime length]==0) {
        headerTitle =  [NSString stringWithFormat:@"%@, %@",formattedDate, startTime];
    }else{
        headerTitle =  [NSString stringWithFormat:@"%@, %@ - %@",formattedDate, startTime, endTime];
    }
    return headerTitle;
   // return [dictionaryKeysArray objectAtIndex:section];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSString *sectionTitle = [dictionaryKeysArray objectAtIndex:section];
    NSArray *newArray= [sessionDictionary objectForKey:sectionTitle];
    return [newArray count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *simpleTableIdentifier = @"SessionListTableViewCell";
    cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SessionListTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    [cell.btnCheck addTarget:self action:@selector(toggleButton) forControlEvents:UIControlEventTouchUpInside];
    [self setUpCell:cell atIndexPath:indexPath];
    [cell.contentView setNeedsLayout];
    [cell.contentView layoutIfNeeded];
    return cell;
}

- (void)setUpCell:(SessionListTableViewCell *)sessionCell atIndexPath:(NSIndexPath *)indexPath {
    NSString *sectionTitle = [dictionaryKeysArray objectAtIndex:indexPath.section];
    Session *session = [sessionDictionary objectForKey:sectionTitle][indexPath.row] ;
    sessionCell.selectionStyle = UITableViewCellSelectionStyleNone;
    sessionCell.lblSessionTitle.text = session.session;
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor =TABLE_HEADER_COLOR;
    
    UILabel *timeLabel = [[UILabel alloc] init];
    timeLabel.frame = CGRectMake(20, 0, 250,32);
    timeLabel.font = [UIFont boldSystemFontOfSize:13];
    timeLabel.textColor = HEADER_TEXT_COLOR;
    timeLabel.text = [self tableView:tableView titleForHeaderInSection:section];
    [headerView addSubview:timeLabel];
    
    return headerView;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 35.0;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    sessionToScan = [[Session alloc]init];
    NSString *sectionTitle = [dictionaryKeysArray objectAtIndex:indexPath.section];
    sessionToScan = [sessionDictionary objectForKey:sectionTitle][indexPath.row];
    [self scanQRCodeWithSession:sessionToScan];
    self.navigationItem.title = @"Scan QR Code";
  //   [self goToPointsController:nil];
}
-(void)scanQRCodeWithSession:(Session *)session{
    if ([QRCodeReader supportsMetadataObjectTypes:@[AVMetadataObjectTypeQRCode]]) {
        static QRCodeReaderViewController *vc = nil;
        static dispatch_once_t onceToken;
        
        dispatch_once(&onceToken, ^{
            QRCodeReader *reader = [QRCodeReader readerWithMetadataObjectTypes:@[AVMetadataObjectTypeQRCode]];
            vc = [QRCodeReaderViewController readerWithCancelButtonTitle:@"Cancel" codeReader:reader startScanningAtLoad:YES showSwitchCameraButton:NO showTorchButton:YES];
            vc.modalPresentationStyle = UIModalPresentationFormSheet;
        });
        vc.delegate = self;
        
        [vc setCompletionWithBlock:^(NSString *resultAsString) {
            // NSLog(@"Completion with result: %@", resultAsString);
        }];
        
        [self presentViewController:vc animated:YES completion:NULL];
    }
    else {
        [self showAlertMessage:@"Reader not supported by the current device" withTitle:@"Error" withTag:ALERT_TAG_ERROR];
    }
    
    
}
-(void)showToastForSuccessfulRegistration{
    self.viewSuccessfullyRegistered.hidden = NO;
    [NSTimer scheduledTimerWithTimeInterval:3.0
                                     target:self
                                   selector:@selector(removeViewSuccess)
                                   userInfo:nil
                                    repeats:NO];
    
}
-(void)removeViewSuccess{
    self.viewSuccessfullyRegistered.hidden = YES;
}
- (void)showAlertMessage:(NSString*)message withTitle:(NSString*)title withTag:(int)tag{
    UIAlertController * chooseSessionAlert =   [UIAlertController
                                                alertControllerWithTitle:title
                                                message:message
                                                preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             self.navigationItem.title = @"Select a Session";
                             
                         }];
    [chooseSessionAlert addAction:ok]; // add action to uialertcontroller
    
}
#pragma mark - QRCodeReader Delegate Methods

-(void)reader:(QRCodeReaderViewController *)reader didScanResult:(NSString *)result{
    
    NSString *qrCodeScannedString = result;
    [self dismissViewControllerAnimated:YES completion:^{
        if ([qrCodeScannedString length] != 0) {
            NSArray* profileArr = [qrCodeScannedString componentsSeparatedByString: @"\n"];
            // [self postQRCodeInfoToSpreadsheet:profileArr];
            [self goToPointsController:profileArr];
            
        }
        self.navigationItem.title = @"Select a Session";
    }];
}
- (void)readerDidCancel:(QRCodeReaderViewController *)reader{
    [self dismissViewControllerAnimated:YES completion:NULL];
    self.navigationItem.title = @"Select a Session";
}
#pragma mark- SessionListDelegate
-(void)isRegistrationSuccessful:(BOOL)result{
    if (result == YES) {
        [self showToastForSuccessfulRegistration];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
