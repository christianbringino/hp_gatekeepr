//
//  PointsViewController.m
//  EESY Breakout
//
//  Created by Kristine Andrada on 5/2/18.
//  Copyright © 2018 Kristine Andrada. All rights reserved.
//

#import "PointsViewController.h"
#import "WebServicesManager.h"
#import "UserDefaultsHelper.h"

@interface PointsViewController ()
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
@property (weak, nonatomic) IBOutlet UITextField *txtPoints;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topSubmit;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UILabel *lblPoints;

@end

@implementation PointsViewController{
    NSString *fullName;
    NSString *emailStr;
    NSString *contactNumStr;
    NSString *companyStr;
    NSString *positionStr;
     BOOL keyboardIsShown;
    UITextField *activeField;
    NSString *projectId;
    NSString *userId;
    NSString *eventCode;
    NSString *stubValue;
    NSString *stubLabel;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpUI];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    [self createLoadingScreen];
    [self displayDetails];
    self.txtPoints.text = @"1";
    self.txtPoints.delegate = self;
    
    
    // Do any additional setup after loading the view.
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [_txtPoints becomeFirstResponder];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self registerForKeyboardNotifications];
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self deregisterFromKeyboardNotifications];
    [self.view endEditing:YES];
    
}
- (void)viewWillLayoutSubviews{
    CGRect scrollFrame = self.scrollView.frame;
    float scrollHeight  = self.lblName.frame.size.height + self.lblEmail.frame.size.height + self.lblCompany.frame.size.height + self.lblContact.frame.size.height + self.lblJobTitle.frame.size.height + self.lblName.frame.size.height + _txtPoints.frame.size.height +_btnSubmit.frame.size.height + 123;
    self.scrollView.frame = scrollFrame;
    self.scrollView.contentSize =CGSizeMake(self.scrollView.frame.size.width, scrollHeight);
}

#pragma mark - IB Actions
- (IBAction)submitAction:(id)sender {
    if (stubValue != nil ) {
        int intStub = [stubValue intValue];
        if (intStub > 0) {
            [self postQRCodeInfoToSpreadsheet];
            [self postStubToFirebase];
          
        }else{
            [self showErrorMessage:[NSString stringWithFormat:@"%@ has no more %@.",fullName,[stubLabel lowercaseString]] andTitle:@"Oops!"];
            // Show alert
        }
    }else{
        [self postQRCodeInfoToSpreadsheet];
    }
   
}
#pragma mark - Private Methods
-(void)setUpUI{
   
    if ( IDIOM == IPAD ) {
        self.topSubmit.constant = 25;
    }

    self.txtPoints.delegate = self;
    [self.btnSubmit setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
}
-(void)dismissKeyboard{
    if ([_txtPoints.text isEqualToString:@"1"]) {
         self.txtPoints.text = @"1";
    }
    [self.txtPoints resignFirstResponder];
}
-(void)displayDetails{
    NSString *firstNameStr;
    NSString *lastNameStr;
    emailStr = @"None";
    contactNumStr = @"None";
    companyStr = @"None";
    positionStr = @"None";
    for (NSString *item in self.contactArr){
        if ([item rangeOfString:@"FN"].location != NSNotFound) {
            NSArray *firstNameArr= [item componentsSeparatedByString:@":"];
            firstNameStr = [firstNameArr lastObject];
        }
        if ([item rangeOfString:@"LN"].location != NSNotFound) {
            NSArray *lastNameArr= [item componentsSeparatedByString:@":"];
            lastNameStr = [lastNameArr lastObject];
        }
        if ([item rangeOfString:@"EMAIL"].location != NSNotFound) {
            NSArray *emailArr= [item componentsSeparatedByString:@":"];
            emailStr = [emailArr lastObject];
            emailStr = [[emailStr componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@" "];
        }
        if ([item rangeOfString:@"TEL"].location != NSNotFound) {
            NSArray *contactNumArr= [item componentsSeparatedByString:@":"];
            contactNumStr = [contactNumArr lastObject];
            contactNumStr= [[contactNumStr componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@" "];
        }
        if ([item rangeOfString:@"COMPANY"].location != NSNotFound) {
            NSArray *companyArr= [item componentsSeparatedByString:@":"];
            companyStr = [companyArr lastObject];
            companyStr = [[companyStr componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@" "];
        }
        if ([item rangeOfString:@"POSITION"].location != NSNotFound) {
            NSArray *positionArr= [item componentsSeparatedByString:@":"];
            positionStr = [positionArr lastObject];
            positionStr = [[positionStr componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@" "];
        }
        if ([item rangeOfString:@"PROJECTID"].location != NSNotFound) {
            NSArray *projectIdArr= [item componentsSeparatedByString:@":"];
            projectId= [projectIdArr lastObject];
            projectId = [[projectId componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@" "];
        }
        if ([item rangeOfString:@"USERID"].location != NSNotFound) {
            NSArray *userIdArr= [item componentsSeparatedByString:@":"];
            userId = [userIdArr lastObject];
            userId = [[userId componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@" "];
        }
        if ([item rangeOfString:@"EVENTCODE"].location != NSNotFound) {
            NSArray *eventCodeArr= [item componentsSeparatedByString:@":"];
            eventCode = [eventCodeArr lastObject];
            eventCode = [[eventCode componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@" "];
        }
        if ([item rangeOfString:@"STUBVALUE"].location != NSNotFound) {
            NSArray *stubArr= [item componentsSeparatedByString:@":"];
            stubValue = [stubArr lastObject];
            stubValue = [[stubValue componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@" "];
        }
        if ([item rangeOfString:@"STUBLABEL"].location != NSNotFound) {
            NSArray *stubArr= [item componentsSeparatedByString:@":"];
            stubLabel = [stubArr lastObject];
            stubLabel = [[stubLabel componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@" "];
        }
    }
    
    if([firstNameStr length] !=0 && [lastNameStr length] != 0 ){
        fullName = [NSString stringWithFormat:@"%@ %@", firstNameStr, lastNameStr];
        fullName = [[fullName componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@" "];
    }else if([firstNameStr length] !=0 && [lastNameStr length] == 0){
        fullName  = firstNameStr;
        fullName = [[fullName componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@" "];
    }else if([firstNameStr length] ==0 && [lastNameStr length] != 0){
        fullName  = lastNameStr;
        fullName = [[fullName componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@" "];
    }else{
        fullName = @"None";
    }
    self.lblName.text = fullName;
    self.lblContact.text = contactNumStr;
    self.lblCompany.text = companyStr;
    self.lblJobTitle.text = positionStr;
    self.lblEmail.text = emailStr;
}
-(void)postStubToFirebase{
    if ([projectId length] != 0 && [eventCode length]!=0 && [userId length]!= 0 && [stubValue length] != 0) {
        NSString *url = [NSString stringWithFormat:@"https://us-central1-%@.cloudfunctions.net/stub/postStub/%@/%@",projectId,eventCode,userId];
        [[WebServicesManager sharedManager] postDataToSpreadsheet:url withDataParams:nil withCallback:^(NSError *error) {
            if (!error) {
                NSLog(@"SUCCESS SENDING STUB COUNT");
            }else{
                [self showErrorMessage: @"An error was encountered while updating credits" andTitle:@"Error"];
            }
        }];
    }
}
-(void)postQRCodeInfoToSpreadsheet{
    NSString *scannerName = [UserDefaultsHelper getScannerName];
    if ([scannerName length]==0) {
        scannerName = @"None";
    }
    NSString *points = _txtPoints.text;
    if ([points length] == 0) {
        points = @"0";
    }
    [self loadSpinner];
    [self dismissKeyboard];
    NSDictionary *params = @{
//                             POST_ATTENDEE_NAME : fullName
//                             ,POST_ATTENDEE_EMAIL: emailStr
//                             ,POST_ATTENDEE_CONTACT : contactNumStr
//                             ,POST_ATTENDEE_COMPANY : companyStr
//                             ,POST_ATTENDEE_POSITION : positionStr
//                             ,POST_ATTENDEE_SCANNER : scannerName
//                             ,POST_ATTENDEE_POINTS : points
                             };
    
    [[WebServicesManager sharedManager] postDataToSpreadsheet:self.attendanceUrl withDataParams:params withCallback:^(NSError *error) {
        [self stopSpinner];
        if (!error) {
            [self.delegate isRegistrationSuccessful:YES];
            [self.navigationController popViewControllerAnimated:YES];
        }else{
                        if (![Reachability isInternetAvailable]) {
                            [self showErrorMessage: @"The internet connection appears to be offline." andTitle:@"Error"];
                        }else{
                             [self showErrorMessage: @"Registration Failed. Please try again." andTitle:@"Error"];
                        }
        }
    }];
}
-(void)showErrorMessage:(NSString *)message andTitle:(NSString *)title{
    UIAlertController *errorAlert =   [UIAlertController
                                                alertControllerWithTitle:title
                                                message:message
                                                preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                              [_txtPoints becomeFirstResponder];
                         }];
    [errorAlert addAction:ok];
     [self presentViewController:errorAlert animated:YES completion:nil];
   
}
#pragma mark - UITextField Delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    activeField = textField;
   // textField.text = @"";
    [textField setKeyboardType:UIKeyboardTypeNumberPad];
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    activeField = nil;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    return YES;
}
- (void)registerForKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)deregisterFromKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidShowNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}
-(void)setScrollViewBack{
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, 0, 0.0);
    _scrollView.contentInset = contentInsets;
    _scrollView.scrollIndicatorInsets = contentInsets;
    keyboardIsShown = NO;
}
#pragma mark Keyboard Delegate Methods

- (void)keyboardWillHide:(NSNotification *)n{
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, 0, 0.0);
    _scrollView.contentInset = contentInsets;
    _scrollView.scrollIndicatorInsets = contentInsets;
    keyboardIsShown = NO;
}

- (void)keyboardWillShow:(NSNotification *)n{
    if (keyboardIsShown){ return; }
    
    NSDictionary* info = [n userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height+65, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your app might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
        [self.scrollView scrollRectToVisible:activeField.frame animated:YES];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
