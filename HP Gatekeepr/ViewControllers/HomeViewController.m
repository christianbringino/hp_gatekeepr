//
//  HomeViewController.m
//  EESY Breakout
//
//  Created by Kristine Andrada on 10/17/16.
//  Copyright © 2016 Kristine Andrada. All rights reserved.
//

#import "HomeViewController.h"
#import "SessionListViewController.h"
#import "LocalDataManager.h"
#import "Constants.h"
#import "Event.h"
#import "WebServicesManager.h"
#import "DateTimeManager.h"
#import "PWGoogleSpreadsheetParser.h"
#import "UserDefaultsHelper.h"
#import "GoToNextTextFieldModule.h"

@interface HomeViewController ()
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIImageView *imgBanner;
@property (weak, nonatomic) IBOutlet UILabel *lblEnterQRCode;
@property (weak, nonatomic) IBOutlet UIButton *btnGetStarted;

@end

@implementation HomeViewController{
    NSArray *eventsArr;
     BOOL keyboardIsShown;
     UITextField *activeField;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _txtEventCode.delegate = self;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    [self validateTextFields];
    [self downloadEvents];
    keyboardIsShown = NO;
     [self registerForKeyboardNotifications];
    [self createLoadingScreen];
  
    // Do any additional setup after loading the view.
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setUpNavBarFontAndBG];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self deregisterFromKeyboardNotifications];
}
-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    
    CGRect scrollFrame = self.scrollView.frame;
    self.scrollView.frame = scrollFrame;
    //6 -380
    float scrollHeight = self.view.frame.size.height *0.6;
    self.scrollView.contentSize =CGSizeMake(self.scrollView.frame.size.width, scrollHeight);
}

#pragma mark - IB Actionsf
- (IBAction)letsGoAction:(id)sender {
//    [self getEventDetails];
    [self fetchGuardCodes];
}

- (void) fetchGuardCodes {
    NSString *url = @"https://spreadsheets.google.com/feeds/list/1uUj0W3XDu9IzFbm6rzrQBlnVDGk2CiyzvbvzFW7mYQE/1/public/values?alt=json";
    
    [self loadSpinner];
    [[WebServicesManager sharedManager] getDataFromSpreadsheet:url withCallback:^(NSDictionary *data, NSError *error) {
        [self stopSpinner];
        //      [spinner stopAnimating];
        //      self.navigationItem.rightBarButtonItem = refresh;
        if ((error == nil) && (data != nil)) {
            NSDictionary *feed = [data objectForKey:@"feed"];
            NSDictionary *updated = [feed objectForKey:@"updated"];
//            NSDate *newDate = [updated objectForKey:@"$t"];
            
            NSArray<NSDictionary *> *arrDic = [feed objectForKey:@"entry"];
            
            for (NSDictionary *dic in arrDic) {
                NSDictionary *dicCode = [dic objectForKey:@"gsx$code"];
                NSString *strCode = [dicCode objectForKey:@"$t"];
                
                if (strCode == _txtEventCode.text) {
                    [UserDefaultsHelper saveCodeAcceptedState:true];
                    [UserDefaultsHelper saveScannerGuardCode:strCode];
                    _txtEventCode.text = @"";
                    [self performSegueWithIdentifier:@"showRooms" sender:nil];
                    return;
                }
            }
            
            [self showAlertWithTitle:@"Error" message:@"The code you input can't be found"];
            
//            NSDate *oldDate = (NSDate *)[[NSUserDefaults standardUserDefaults] objectForKey:LAST_UPDATED_EVENTS_KEY];
//            NSArray *temp = [LocalDataManager fetchDatafrom:FILE_EVENT_KEY];
//            if (oldDate && temp) {
//                if ([DateTimeManager isNewDate:newDate newerThanOldDate:oldDate]) {
//                    [LocalDataManager removeFilesAtFilePath:FILE_EVENT_KEY];
//                    [LocalDataManager saveData:[PWGoogleSpreadsheetParser parseResponseObject:data] to:FILE_EVENT_KEY];
//                    [[NSUserDefaults standardUserDefaults] setObject:newDate forKey:LAST_UPDATED_EVENTS_KEY];
//                }
//            }
//            else{
//                [LocalDataManager saveData:[PWGoogleSpreadsheetParser parseResponseObject:data] to:FILE_EVENT_KEY];
//                [[NSUserDefaults standardUserDefaults] setObject:newDate forKey:LAST_UPDATED_EVENTS_KEY];
//
//            }
        }
        
        else {
            
        }
        
    }];
}

- (IBAction)txtFieldAction:(id)sender {
    [self validateTextFields];
}

#pragma mark - Private Methods
-(void) validateTextFields {
    
    if (_txtEventCode.text.length > 0) {
        self.btnGetStarted.enabled = YES;
        self.btnGetStarted.alpha = 1.0;
    }else {
        [self.btnGetStarted setEnabled:NO];
        self.btnGetStarted.alpha = 0.54;
    }
}

- (void)downloadEvents{
    NSString *url = [@[GGL_SPRDSHT_DATA_BASE_URL,GGL_SPRDSHT_ID_EVENTS,GGL_SPRDSHT_WORKSHEET_DATA,GGL_SPRDSHT_DATA_REST_PARAMS] componentsJoinedByString:@"/"];
    [[WebServicesManager sharedManager] getDataFromSpreadsheet:url withCallback:^(NSDictionary *data, NSError *error) {
        //      [spinner stopAnimating];
        //      self.navigationItem.rightBarButtonItem = refresh;
        if ((error == nil) && (data != nil)) {
            NSDictionary *feed = [data objectForKey:@"feed"];
            NSDictionary *updated = [feed objectForKey:@"updated"];
            NSDate *newDate = [updated objectForKey:@"$t"];
            
            NSDate *oldDate = (NSDate *)[[NSUserDefaults standardUserDefaults] objectForKey:LAST_UPDATED_EVENTS_KEY];
            NSArray *temp = [LocalDataManager fetchDatafrom:FILE_EVENT_KEY];
            if (oldDate && temp) {
                if ([DateTimeManager isNewDate:newDate newerThanOldDate:oldDate]) {
                    [LocalDataManager removeFilesAtFilePath:FILE_EVENT_KEY];
                    [LocalDataManager saveData:[PWGoogleSpreadsheetParser parseResponseObject:data] to:FILE_EVENT_KEY];
                    [[NSUserDefaults standardUserDefaults] setObject:newDate forKey:LAST_UPDATED_EVENTS_KEY];
                }
            }
            else{
                [LocalDataManager saveData:[PWGoogleSpreadsheetParser parseResponseObject:data] to:FILE_EVENT_KEY];
                [[NSUserDefaults standardUserDefaults] setObject:newDate forKey:LAST_UPDATED_EVENTS_KEY];
                
            }
        }
        
        else {
            
        }
        
    }];
}

-(void)getEventDetails{
    if(![Reachability isInternetAvailable]){
        [self displayErrorMessage:@"Please check your network connection and try again." andTitle:@"No Network Connection"];
    }else{
    eventsArr = [LocalDataManager fetchDatafrom:FILE_EVENT_KEY];
    NSMutableArray *tempArr = [[NSMutableArray alloc]init];
    
    for (NSDictionary *item in eventsArr) {
        Event *event = [[Event alloc]initWithDataDictionary:item];
        if ([event.code isEqualToString:_txtEventCode.text]) {
            [tempArr addObject:event.code];
            [UserDefaultsHelper saveCodeAcceptedState:YES];
                SessionListViewController *slvc = [self.storyboard instantiateViewControllerWithIdentifier:@"SessionListViewController"];
               [self setBackBarButtonTitleTo:@""];
            [UserDefaultsHelper saveEventInfoUrl:event.url];
            
                slvc.eventUrl = event.url;
       //    [self presentViewController:slvc animated:YES completion:nil];
            [self.navigationController pushViewController:slvc animated:YES];
            break;
        }
        [tempArr addObject:event.code];
       
    }
    if (![tempArr containsObject:_txtEventCode.text]){
        [self displayErrorMessage:@"The event code you entered is invalid. Please try again." andTitle:@"Invalid Event Code"];
        //alert
    }
    }

}
- (void)dismissKeyboard{
    [_txtEventCode resignFirstResponder];
 
}
-(void)displayErrorMessage:(NSString *)message andTitle:(NSString *)title{
    UIAlertController * invalidCodeAlert =   [UIAlertController
                                                alertControllerWithTitle:title
                                                message:message
                                                preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             //Do some thing here
                             // [view dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    [invalidCodeAlert addAction:ok]; // add action to uialertcontroller

    
    [self presentViewController:invalidCodeAlert animated:YES completion:nil];
}

#pragma mark - UITextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    activeField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    activeField = nil;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [GoToNextTextFieldModule goToNextFieldWithTextField:textField];
    return NO;
}
#pragma mark - Keyboard Notification

- (void)keyboardWillHide:(NSNotification *)n{
    
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    _scrollView.contentInset = contentInsets;
    _scrollView.scrollIndicatorInsets = contentInsets;
    keyboardIsShown = NO;
}
- (void)keyboardWillShow:(NSNotification *)n{
    if (keyboardIsShown){ return; }
    
    NSDictionary* info = [n userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height+55, 0.0);
    _scrollView.contentInset = contentInsets;
    _scrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
        [self.scrollView scrollRectToVisible:activeField.frame animated:YES];
    }
    keyboardIsShown = YES;
}

- (void)registerForKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)deregisterFromKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidShowNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
