//
//  SessionListViewController.h
//  EESY Breakout
//
//  Created by Kristine Andrada on 10/17/16.
//  Copyright © 2016 Kristine Andrada. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PWBaseViewController.h"
#import "QRCodeReaderViewController.h"
#import "SessionListDelegate.h"

@interface SessionListViewController : PWBaseViewController<UITableViewDataSource, UITableViewDelegate,QRCodeReaderDelegate, SessionListDelegate>
@property (weak, nonatomic) IBOutlet UITableView *sessionTableView;
@property (strong, nonatomic) NSString *eventUrl;

@end
