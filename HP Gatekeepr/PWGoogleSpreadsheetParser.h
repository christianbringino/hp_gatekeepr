//
//  Parser.h
//  Pocket Rocket
//
//  Created by ios-training on 6/24/15.
//  Copyright (c) 2015 ios-training. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"

@interface PWGoogleSpreadsheetParser : NSObject

+ (NSArray*)parseResponseObject:(id)responseObject;
+ (NSArray*)parseResponseObject:(id)responseObject forUser:(NSString *)user;
@end
