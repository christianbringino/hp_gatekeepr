//
//  SessionListTableViewCell.h
//  EESY Breakout
//
//  Created by Kristine Andrada on 10/17/16.
//  Copyright © 2016 Kristine Andrada. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SessionListTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblSessionTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnCheck;


@end
