//
//  SessionListTableViewCell.m
//  EESY Breakout
//
//  Created by Kristine Andrada on 10/17/16.
//  Copyright © 2016 Kristine Andrada. All rights reserved.
//

#import "SessionListTableViewCell.h"

@implementation SessionListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
