//
//  Parser.m
//  Pocket Rocket
//
//  Created by ios-training on 6/24/15.
//  Copyright (c) 2015 ios-training. All rights reserved.
//

#import "PWGoogleSpreadsheetParser.h"

@implementation PWGoogleSpreadsheetParser

+ (NSArray*)parseResponseObject:(id)responseObject {
    
    NSDictionary *feed = [responseObject objectForKey:@"feed"];
    NSArray *entries   = [feed objectForKey:@"entry"];
    
    NSMutableArray *dataArr = [[NSMutableArray alloc] init];
    
    for (NSDictionary *entry in entries) {
        
        NSMutableDictionary *tempDict = [NSMutableDictionary dictionary];
        
        [entry enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            if ([key rangeOfString:@"gsx$"].length != 0) {
                [tempDict setObject:[obj objectForKey:@"$t"] forKey:key];
            }
        }];
        
        [dataArr addObject:tempDict];
    }
    
    return dataArr;
}

+ (NSArray*)parseResponseObject:(id)responseObject forUser:(NSString *)user{
    
    NSDictionary *feed = [responseObject objectForKey:@"feed"];
    NSArray *entries   = [feed objectForKey:@"entry"];
    
    NSMutableArray *dataArr = [[NSMutableArray alloc] init];
    
    for (NSDictionary *entry in entries) {
        
        NSMutableDictionary *tempDict = [NSMutableDictionary dictionary];
        
        [entry enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            if ([key rangeOfString:@"gsx$"].length != 0) {
                [tempDict setObject:[obj objectForKey:@"$t"] forKey:key];
            }
        }];
        
        if ([[[tempDict objectForKey:@"gsx$speaker"] lowercaseString] rangeOfString:[user lowercaseString]].location == NSNotFound) {
            //do nothing
        } else {
            [dataArr addObject:tempDict];
        }
        
    }
    
    
    return dataArr;
}
@end
