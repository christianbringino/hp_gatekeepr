//
//  Room.m
//  HP Gatekeepr
//
//  Created by Christian Bringino on 8/25/20.
//  Copyright © 2020 Kristine Andrada. All rights reserved.
//

#import "Room.h"

@implementation Room

-(instancetype)initWithDataDictionary:(NSDictionary *)dataDict{
    self = [super init];
    if (self) {
        NSDictionary *dicRoom = [dataDict objectForKey:@"gsx$room"];
        NSDictionary *dicDescription = [dataDict objectForKey:@"gsx$descrption"];
        
        _room = [dicRoom objectForKey:@"$t"];
        _descriptions = [dicDescription objectForKey:@"$t"];
    }
    return self;
}

@end
