//
//  StringFormatter.h
//  EESY
//
//  Created by Kristine Andrada on 14/07/2016.
//  Copyright © 2016 ios-training. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StringFormatter : NSObject
+(NSString *)checkEmptyString:(NSString *)string;
+(NSString *)changeDateStringFormatWithYear:(NSString *)dateString;
+(NSString *)changeDateStringFormat:(NSString *)dateString;
@end
