//
//  UserDefaultsHelper.m
//  EESY Breakout
//
//  Created by Kristine Andrada on 10/17/16.
//  Copyright © 2016 Kristine Andrada. All rights reserved.
//

#import "UserDefaultsHelper.h"

@implementation UserDefaultsHelper
+ (NSString *)getScannerGuardCode {
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"guardCode"];
}

+ (void)saveScannerGuardCode: (NSString *) code {
    [[NSUserDefaults standardUserDefaults] setValue:code forKeyPath:@"guardCode"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)saveSessionSelectState:(BOOL)status {
    [[NSUserDefaults standardUserDefaults] setBool:status forKey:@"isSessionSelected"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (BOOL)getSessionSelectState {
    BOOL isWifiOn = [[NSUserDefaults standardUserDefaults] boolForKey:@"isSessionSelected"];
    return isWifiOn;
}
+ (void)saveCodeAcceptedState:(BOOL)status {
    [[NSUserDefaults standardUserDefaults] setBool:status forKey:@"isCodeAccepted"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (BOOL)getCodeAcceptedState {
    BOOL isCodeAccepted= [[NSUserDefaults standardUserDefaults] boolForKey:@"isCodeAccepted"];
    return isCodeAccepted;
}
+ (void)saveEventInfoUrl:(NSString *)url {
    [[NSUserDefaults standardUserDefaults] setObject:url forKey:@"eventInfoUrl"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)getEventInfoUrl {
    NSString *url = [[NSUserDefaults standardUserDefaults] objectForKey:@"eventInfoUrl"];
    return url;
}
+ (void)saveScannerName:(NSString *)name {
    [[NSUserDefaults standardUserDefaults] setObject:name forKey:@"scannerName"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)getScannerName {
    NSString *name = [[NSUserDefaults standardUserDefaults] objectForKey:@"scannerName"];
    return name;
}

@end
