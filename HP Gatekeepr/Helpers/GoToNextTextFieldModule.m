//
//  GoToNextTextFieldModule.m
//  Pocket Rocket
//
//  Created by ios-training on 7/20/15.
//  Copyright (c) 2015 ios-training. All rights reserved.
//

#import "GoToNextTextFieldModule.h"

@implementation GoToNextTextFieldModule


+ (void)goToNextFieldWithTextField:(UITextField *)textField {
    NSInteger textFieldTag = textField.tag;
    
    UIView *nextObject = [textField.superview viewWithTag:textFieldTag + 1];
    
    BOOL isNextTextFieldATextField = [nextObject isKindOfClass:[UITextField class]];
    
    if ( isNextTextFieldATextField ) {
        [textField resignFirstResponder];
        [nextObject becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
}
@end
