//
//  GoToNextTextFieldModule.h
//  Pocket Rocket
//
//  Created by ios-training on 7/20/15.
//  Copyright (c) 2015 ios-training. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface GoToNextTextFieldModule : NSObject

/*!
 * Will go to the next textfield based on it's tag so make sure you supply tags incremented by 1 on your textfields. You will call this inside of the delegate method textFieldShouldReturn.
 * @param textField Pass the textField parameter from the textFieldShouldReturn method to this.
 *
 */
+ (void)goToNextFieldWithTextField:(UITextField *)textField;
@end
