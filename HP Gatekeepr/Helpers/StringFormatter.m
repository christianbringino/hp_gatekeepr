//
//  StringFormatter.m
//  EESY
//
//  Created by Kristine Andrada on 14/07/2016.
//  Copyright © 2016 ios-training. All rights reserved.
//

#import "StringFormatter.h"

@implementation StringFormatter
+(NSString *)checkEmptyString:(NSString *)string{
    if ([string length] == 0) {
        string = @"none";
    }
    return string;
}

+(NSString *)changeDateStringFormat:(NSString *)dateString{
    NSArray *dateArr = [dateString componentsSeparatedByString:@"/"];
    long month = [[dateArr objectAtIndex:0] integerValue];
    NSString *date = [dateArr objectAtIndex:1];
    NSString *convertedDateString = [NSString stringWithFormat:@"%@ %@", [self convertDateNumToWord:month], date];
    return convertedDateString;
    
}
+(NSString *)changeDateStringFormatWithYear:(NSString *)dateString{
    NSArray *dateArr = [dateString componentsSeparatedByString:@"/"];
    long month = [[dateArr objectAtIndex:0] integerValue];
    NSString *date = [dateArr objectAtIndex:1];
    NSString *year = [dateArr objectAtIndex:2];
    NSString *convertedDateString = [NSString stringWithFormat:@"%@ %@, %@", [self convertDateNumToWord:month], date, year];
    return convertedDateString;
    
}

+(NSString *)convertDateNumToWord:(long)num{
    NSString *month;
    switch (num) {
        case 1:
            month = @"Jan";
            break;
        case 2:
            month = @"Feb";
            break;
        case 3:
            month = @"Mar";
            break;
        case 4:
            month = @"Apr";
            break;
        case 5:
            month = @"May";
            break;
        case 6:
            month = @"Jun";
            break;
        case 7:
            month = @"Jul";
            break;
        case 8:
            month = @"Aug";
            break;
        case 9:
            month = @"Sept";
            break;
        case 10:
            month = @"Oct";
            break;
        case 11:
            month = @"Nov";
            break;
        case 12:
            month = @"Dec";
            break;
            
        default:
            break;
            
    }
    return month;
    
}

@end
