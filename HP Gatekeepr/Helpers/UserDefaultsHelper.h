//
//  UserDefaultsHelper.h
//  EESY Breakout
//
//  Created by Kristine Andrada on 10/17/16.
//  Copyright © 2016 Kristine Andrada. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserDefaultsHelper : NSObject
+ (void)saveScannerGuardCode: (NSString *) code;
+ (NSString *)getScannerGuardCode;
+ (BOOL)getSessionSelectState;
+ (void)saveSessionSelectState:(BOOL)status;
+ (void)saveCodeAcceptedState:(BOOL)status;
+ (BOOL)getCodeAcceptedState;
+ (void)saveEventInfoUrl:(NSString *)url;
+ (NSString *)getEventInfoUrl;
+ (void)saveScannerName:(NSString *)name;
+ (NSString *)getScannerName;
@end
