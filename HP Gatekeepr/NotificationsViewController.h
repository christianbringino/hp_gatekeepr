//
//  NotificationsViewController.h
//  HP Gatekeepr
//
//  Created by Christian Bringino on 8/25/20.
//  Copyright © 2020 Kristine Andrada. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PWBaseViewController.h"
@import FirebaseDatabase;

NS_ASSUME_NONNULL_BEGIN

@interface NotificationsViewController : PWBaseViewController
@property (strong, nonatomic) FIRDatabaseReference *ref;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

NS_ASSUME_NONNULL_END
