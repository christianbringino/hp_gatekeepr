//
//  ScannedUserDetailsViewController.h
//  HP Scan
//
//  Created by Christian Bringino on 6/30/20.
//  Copyright © 2020 Kristine Andrada. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ScannedUserDetailsViewController : PWBaseViewController

@property (strong, nonatomic) NSString *userId;
@property (strong, nonatomic) NSString *imageUrl;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSNumber *healthStatus;
@property (strong, nonatomic) NSNumber *timestamp;

@end

NS_ASSUME_NONNULL_END
