//
//  SessionListDelegate.h
//  EESY Breakout
//
//  Created by Kristine Andrada on 5/2/18.
//  Copyright © 2018 Kristine Andrada. All rights reserved.
//

#import<Foundation/Foundation.h>

@protocol SessionListDelegate <NSObject>
-(void)isRegistrationSuccessful:(BOOL)result;
@end
