//
//  main.m
//  EESY Breakout
//
//  Created by Kristine Andrada on 10/13/16.
//  Copyright © 2016 Kristine Andrada. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
