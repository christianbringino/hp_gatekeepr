//
//  TableViewCell.m
//  HP Scan
//
//  Created by Christian Bringino on 6/26/20.
//  Copyright © 2020 Kristine Andrada. All rights reserved.
//

#import "LegendTableViewCell.h"

@implementation LegendTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
