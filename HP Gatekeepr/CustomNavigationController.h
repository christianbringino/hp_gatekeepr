//
//  CustomNavigationController.h
//  HP Scan
//
//  Created by Christian Bringino on 6/30/20.
//  Copyright © 2020 Kristine Andrada. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CustomNavigationController : UINavigationController

@end

NS_ASSUME_NONNULL_END
